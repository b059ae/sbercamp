<?php
use yii\helpers\Html;

$this->title = $subject;
?>

<p>Регистрация участника</p>
<br>
<table border="1">
    <tr>
        <th>ФИО</th>
        <td><?=$model->fio?></td>
    </tr>
    <tr>
        <th>Телефон</th>
        <td><?=$model->phone?></td>
    </tr>
    <tr>
        <th>Email</th>
        <td><?=$model->email?></td>
    </tr>
    <tr>
        <th>Секция</th>
        <td><?=$model->sectionText?></td>
    </tr>
</table>
<p>Посмотреть участника Вы можете <?= Html::a('здесь', $link) ?>.</p>
<hr>
<p>Это автоматическое сообщение и на него не нужно отвечать.</p>