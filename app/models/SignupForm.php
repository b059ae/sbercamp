<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\catalog\models\Category;
use yii\easyii\modules\catalog\models\Item;
use yii\easyii\modules\feedback\models\Feedback;

class SignupForm extends Model
{
    public $fio;
    public $phone;
    public $email;
    public $section;
    public $sectionText;

    const MAX_SPEAKERS = 9;

    protected $_sections;
    public $optionSections=[];

    public function rules()
    {
        return [
            [['fio','phone','email','section'], 'required'],
            [['fio','phone','email'], 'string'],
            [['email'], 'email'],
            [['section'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'fio' => 'Фамилия Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'section' => 'Секция',
        ];
    }

    public function save()
    {
        //тут должна быть проверка на количество участников в секции. Не более 9
        //Получение списка участников
        //$cat = Category::model()->findOne($this->section);
        if (Item::find()->andWhere([
            'category_id' => $this->section,
        ])->count() >= static::MAX_SPEAKERS){
            Yii::$app->session->setFlash('error', 'В этой секции все места заняты. Вы можете записаться на другую.');
            return false;
        }

        $cat = Category::findOne($this->section);
        if (!$cat){
            Yii::$app->session->setFlash('error', 'Секция недоступна. Вы можете записаться на другую.');
            return false;
        }
        $this->sectionText = $cat->title;

        $model = new Feedback([
            'name'=> $this->fio,
            'email'=> $this->email,
            'phone'=> $this->phone,
            'text'=> $this->sectionText,
        ]);

        /*$model = new Item([
            'category_id' => $this->section,
            'time' => time(),
            'available' => 1,
            'title'=> $this->fio,
            'slug'=>uniqid(),
        ]);
        $model->data = [
            'phone'=>$this->phone,
            'email'=>$this->email,
        ];*/

        return $model->save();
    }

    public function fieldOptions($firstOption = '')
    {
        if (!$this->_sections){
            $this->_sections = Catalog::cat('ucastniki')->getChildren();
        }

        $options = [];
        if($firstOption) {
            $options[''] = $firstOption;
        }

        foreach($this->_sections as $field){
            $title = $field->title;
            $extraOptions = [];
            //Считаем сколько уже человек записалось
            if (Item::find()->andWhere([
                    'category_id' => $field->id,
                    'status'=>Item::STATUS_ON,
                ])->count() >= static::MAX_SPEAKERS){
                $extraOptions['disabled']='disabled';
                $title.=' (нет мест)';
            }
            /*$extraOptions['class']='background-color-'.$field->id;*/

            $this->optionSections[$field->id] = $extraOptions;

            $options[$field->id] = $title;
        }
//        var_dump($this->optionSections);
//        die();
        return $options;
    }

    public function fieldDisabledOptions(){
        if (!$this->_sections){
            $this->_sections = Catalog::cat('ucastniki')->getChildren();
        }

        $options = [];

        //Считаем сколько уже человек записалось
        foreach($this->_sections as $field){
            if (Item::find()->andWhere([
                    'category_id' => $field->id,
                ])->count() >= static::MAX_SPEAKERS){

                $options[$field->id] = ['disabled'=>'disabled'];
            }


        }
    }
}