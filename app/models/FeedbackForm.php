<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\catalog\models\Category;
use yii\easyii\modules\catalog\models\Item;
use yii\easyii\modules\feedback\models\Feedback;
use yii\easyii\modules\guestbook\models\Guestbook;

class FeedbackForm extends Model
{
    public $blok;
    public $spiker;
    public $novizna;
    public $polza;
    public $atmo;
    public $org;
    public $isp;
    public $pozh;

    public function rules()
    {
        return [
            [['blok','spiker','novizna','polza', 'atmo', 'org', 'isp'/*, 'pozh'*/], 'required'],
            [['blok','spiker','novizna','polza', 'atmo', 'org', 'isp', 'pozh'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'blok' => 'Программа блог-конференции',
            'spiker' => 'Мастерство спикеров',
            'novizna' => 'Новизна материала',
            'polza' => 'Полезность обучения лично для Вас',
            'atmo' => 'Эмоциональная атмосфера мероприятия',
            'org' => 'Организационное обеспечение мероприятия',
            'isp' => 'Будете ли вы использовать знания, приобретенные здесь? Каким образом?',
            'pozh' => 'Какие пожелания Вы бы оставили организаторам',
        ];
    }

    /**
     * Возможные оценки
     * @return array
     */
    public function marks(){
        $marks = [];
        for($i=1;$i<=10;$i++){
            $marks[$i] = $i;
        }
        return $marks;
    }

    public function save(){
        $model = new Guestbook();
        $model->ip = Yii::$app->request->userIP;
        $model->time = time();
        $model->new = 1;
        $model->status = Guestbook::STATUS_ON;
        $model->name = 'Участник';
        $model->text = "Отзыв участника\n
                        Программа блок-конференции: ".$this->blok."\n
                        Мастерство спикеров: ".$this->spiker."\n
                        Новизна материала: ".$this->novizna."\n
                        Полезность обучения лично для Вас: ".$this->polza."\n
                        Эмоциональная атмосфера мероприятия: ".$this->atmo."\n
                        Организационное обеспечение мероприятия: ".$this->org."\n
                        Будете ли вы использовать знания, приобретенные здесь? Каким образом?\n
                        ".$this->isp."\n
                        Какие пожелания Вы бы оставили организаторам\n
                        ".$this->pozh;
        $model->save();
    }
}