<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/slideh.css',
//        'css/lightbox.min.css',
        'css/colorbox.css',
        'css/style.css',
    ];
    public $js = [
//        'js/lightbox.js',
        //'js/wp-colorbox.js',
//        'js/jquery.colorbox.js',
        'js/jquery.colorbox-min.js',
        'js/slides.min.jquery.js',
        'js/masonry.pkgd.min.js',
        'js/jquery.mosaicflow.min.js',
        'js/jquery.roundabout.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
