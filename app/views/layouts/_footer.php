<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */
?>
<div class="delimiter-line row slide">
    <div class="col-md-12">
        <div class="container">
            <div class="row support-company">
                <div class="col-md-6 col-sm-6">
                    <h2>Организатор</h2>
                    <div class="support-owner">
                        <img src="<?= $asset->baseUrl ?>/img/logo_1.png" class="img-responsive center-block">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h2>При поддержке</h2>
                    <!--<div class="row">
                        <div class="col-md-5 col-md-offset-1 col-xs-12">-->
                    <div class="support-img">
                        <img src="<?= $asset->baseUrl ?>/img/hello_b.png" class="img-responsive center-block"/>
                    </div>
                    <!--</div>-->
                    <!--<div class="col-md-5 col-xs-12">
                <div class="support-img text-center">
                    <img src="<? /*= $asset->baseUrl */ ?>/img/kluch.png" class="img-responsive"/>
                </div>
            </div>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<div id="slide-footer" class="row">
    <div class="col-md-12 foot-form">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-6 col-sm-6">
                    <p>
                        Возникли вопросы?<br>
                        Вы можете связаться с<br>
                        организатором
                    </p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <p>
                        <strong>
                            Селиванова Варвара<br>
                        </strong>
                        <a href="mailto:vpselivanova@sberbank.ru">vpselivanova@sberbank.ru</a><br>
                        <a href="tel:+79123952958">8 (918) 855-01-41</a>
                    </p>
                    <div class="foot-area">
                        <img src="<?= $asset->baseUrl ?>/img/1_58.png" class="img-responsive"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
