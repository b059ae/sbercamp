<?php
/**
 * @var string $content
 */
use app\widgets\Alert;
use app\assets\AppAsset;
use yii\helpers\Html;

$asset = AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=cyrillic" rel="stylesheet">
        <link rel="shortcut icon" href="<?= $asset->baseUrl ?>/favicon.png" type="image/x-icon">
        <link rel="icon" href="<?= $asset->baseUrl ?>/favicon.png" type="image/x-icon">
        <?php $this->head(); ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>
    <div id="toTop"></div>
    <div class="container-fluid nopadding">
        <?= $this->render('_header', ['asset' => $asset]); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <?= Alert::widget([
                                        'alertTypes' => [
                                            'error' => 'bg-danger',
                                            'danger' => 'bg-danger',
                                            'success' => 'bg-success',
                                            'warning' => 'bg-warning',
                                        ],
                                        'closeButton' => false,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?= $content; ?>
        <?= $this->render('_footer', ['asset' => $asset]); ?>
        <?php $this->endBody(); ?>
    </div>
    </body>
    </html>
<?php $this->endPage() ?>