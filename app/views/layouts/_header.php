<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */

use yii\easyii\modules\menu\api\Menu;

?>
<div class="row">
    <div class="col-md-12">
        <header id="top" class="navbar navbar-fixed-top navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="menu-icon navbar-toggle collapsed" data-toggle="collapse" data-target="#top-menu" aria-controls="top-menu" aria-expanded="false">
                        <span class="sr-only">Меню</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="<?= $asset->baseUrl ?>/img/logo_1.png" alt="SBERCAMP"></a>
                </div>

                <nav id="top-menu" class="collapse navbar-collapse">
                    <ul id="menu-tabs" class="nav navbar-nav navbar-right">
                        <?php foreach (Menu::items('main') as $item) : ?>
                            <li>
                                <a href="<?=$item['url']?>"><?=$item['label']?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <div id="menu-tabs" class="transform-reg">
                    <a type="submit" class="absolute-position button-reg" href="/#ocenit" >
                        <p class="hidden-xs">ОЦЕНИТЬ МЕРОПРИЯТИЕ</p>
                        <img src="<?= $asset->baseUrl ?>/img/pencil.png" class="hidden-md hidden-lg hidden-sm">
                    </a>
                </div>
            </div>
        </header>
    </div>
</div>

<div id="slide-main" class="row delimiter-line">
    <div class="col-md-12">
        <div class="main-form container">
            <img src="<?= $asset->baseUrl ?>/img/main.png" class="img-responsive"/>

            <!--<div class="row">
                <div class="col-md-8">
                    <div class="sec1_image1">
                        <img src="<?/*= $asset->baseUrl */?>/img/sbercamp_1.png" class="img-responsive"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="sec1_image1">
                        <img src="<?/*= $asset->baseUrl */?>/img/59.png" class="img-responsive"/>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43677479 = new Ya.Metrika({
                    id:43677479,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43677479" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-96116304-1', 'auto');
    ga('send', 'pageview');

</script>