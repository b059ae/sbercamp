<?php
/** @var $page \yii\easyii\modules\page\api\PageObject */
/** @var $gallery \yii\easyii\modules\gallery\api\PhotoObject[] */
/** @var $spikery \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var $raspisanie \yii\easyii\modules\entity\api\ItemObject */
/** @var $video \yii\easyii\modules\carousel\api\CarouselObject[] */
/** @var $signupForm \app\models\SignupForm */
/** @var $feedbackForm \app\models\FeedbackForm */

$asset = \app\assets\AppAsset::register($this);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title');
?>

    <?= $this->render('slides/about', ['asset' => $asset]); ?>
    <?= $this->render('slides/speaker', ['asset' => $asset, 'spikery'=>$spikery]); ?>
    <?= $this->render('slides/shedule', ['asset' => $asset, 'raspisanie' => $raspisanie]); ?>
    <?php /*= $this->render('slides/signup', ['asset' => $asset, 'signupForm'=>$signupForm]);*/ ?>
    <?= $this->render('slides/feedback', ['asset' => $asset, 'feedbackForm'=>$feedbackForm]); ?>
    <?= $this->render('slides/gallery', ['asset' => $asset, 'gallery'=>$gallery]); ?>
    <?= $this->render('slides/video', ['asset' => $asset, 'video' => $video]); ?>
    <?= $this->render('slides/evolution', ['asset' => $asset]); ?>
    <?= $this->render('slides/address', ['asset' => $asset, 'page'=>$page]); ?>

<?php
$script = <<< JS
    $(".youtube").colorbox({iframe: true, innerWidth: 640, innerHeight: 390, maxWidth:'95%', maxHeight:'95%'});
    $(".photo").colorbox({maxWidth:"80%", maxHeight:"80%", rel:"photo-slide", transition:"fade"});
    $(".vimeo").colorbox({iframe: true, innerWidth: 500, innerHeight: 409});
    $("#click").click(function () {
        $('#click').css({
            "background-color": "#f00",
            "color": "#fff",
            "cursor": "inherit"
        }).text("Open this window again and this message will still be here.");
        return false;
    });
    $('#slides-container').roundabout({
        minScale: 0.8,
        childSelector: "li",
        autoplay: false,
        /*duration:1000,
        autoplayDuration:4000,
        autoplayPauseOnHover: true,*/
        dots: true
    });
    $('#menu-tabs a[href^="/#"]').click(function(e) {
        e.preventDefault();
        var scrollTo = $(this).attr('href').slice(1);
        if ($(scrollTo).length != 0) {
            $("html, body").animate({
                scrollTop: $(scrollTo).offset().top - $(".navbar-fixed-top").height()
            }, 500);
            return false; 
        }
    });
    
    /*Скрол при переходе с другой страницы*/
     // *only* if we have anchor on the url
    if(window.location.hash) {
        // smooth scroll to the anchor id
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top - $(".navbar-fixed-top").height()
        }, 500);
    }
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>