<?php
/** @var $page \yii\easyii\modules\page\api\PageObject */
use yii\easyii\models\Setting;
use yii\helpers\Url;

?>
<div id="kontakty" class="row delimiter-line slide choose-us">
    <div class="col-md-12">
        <h2>Контакты</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <p class="address-p">Адрес:</p>
                            <p>
                                Ставропольский край,<br>
                                г. Кисловодск,<br>
                                Курортный бульвар, 14
                            </p>
                        </div>
                        <div class="col-md-9">
                            <?php
                            $parts = explode("#", $page->data->address);
                            if (count($parts) == 4): ?>
                                <div id="map"></div>
                                <script>

                                    function initMap() {
                                        var pinColor = "83d924";
                                        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                                            new google.maps.Size(31, 44),
                                            new google.maps.Point(0,0),
                                            new google.maps.Point(10, 34),
                                            new google.maps.Size(31, 44));
                                        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
                                            new google.maps.Size(40, 37),
                                            new google.maps.Point(0, 0),
                                            new google.maps.Point(12, 35));


                                        var myLatLng = {lat: <?= $parts[2] ?>, lng: <?= $parts[3] ?>};

                                        var map = new google.maps.Map(document.getElementById('map'), {
                                            zoom: 17,
                                            center: myLatLng
                                        });

                                        var marker = new google.maps.Marker({
                                            position: myLatLng,
                                            map: map,
                                            icon: '<?=Url::to($asset->baseUrl, true)?>/img/marker.png'
                                            /*icon: pinImage,
                                             shadow: pinShadow*/
                                        });
                                    }

                                </script>
                                <script async defer
                                        src="https://maps.googleapis.com/maps/api/js?key=<?= Setting::get('gm_api_key') ?>&signed_in=true&callback=initMap&language=<?= Yii::$app->language ?>"></script>
                                <?php /*
                        <iframe width="100%" height="445" frameborder="0" style="border:0"
                                src="https://www.google.com/maps/embed/v1/place?q=place_id:<?= $parts[1] ?>&key=<?= Setting::get('gm_api_key') ?>&language=<?= Yii::$app->language ?>"
                        ></iframe>
                        */ ?>
                            <?php endif;  ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>