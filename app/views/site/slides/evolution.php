<?php
/** @var $asset \yii\web\AssetBundle */

use yii\helpers\Url;
?>

<div id="istoriya" class="row delimiter-line slide choose-us">
    <div class="col-md-12">
        <h2>История мероприятия SBERCAMP</h2>
<?php /*        <p class="text-center">Вы можете посмотреть всю историю обновлений сайта SBERCAMP.</p> */ ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="timeline gallerycontainer">
                                <ul>
                                    <li>
                                        <div>
                                            <p>Сентябрь 2016</p>
                                            <time>Тюмень</time>
                                            <p><a class="thumb" href="http://2016.sbercamp.ru">ПРОСМОТРЕТЬ<span><img src="<?=Url::to($asset->baseUrl, true)?>/img/thumb-2016.png" /></span></a></p>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <p>Март 2017</p>
                                            <time>Кисловодск</time>
                                            <p><a class="thumb" href="/">ПРОСМОТРЕТЬ<span><img src="<?=Url::to($asset->baseUrl, true)?>/img/thumb-2017.png" /></span></a></p>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <p>Июль 2017</p>
                                            <time>Владивосток</time>
                                            <?php /*<p><a class="thumb" href="http://2018.sbercamp.ru">ПРОСМОТРЕТЬ<span><img src="<?=Url::to($asset->baseUrl, true)?>/img/thumb-2017.png" /></span></a></p>*/?>
                                        </div>
                                    </li>
                                </ul>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<<JS
    (function() {
    
      'use strict';
    
      // define variables
      var items = document.querySelectorAll(".timeline li");
    
      // check if an element is in viewport
      // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
      function isElementInViewport(el) {
        var rect = el.getBoundingClientRect();
        return (
          rect.top >= 0 &&
          rect.left >= 0 &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
          rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
      }
    
      function callbackFunc() {
        for (var i = 0; i < items.length; i++) {
          if (isElementInViewport(items[i])) {
            items[i].classList.add("in-view");
          }
        }
      }
    
      // listen for events
      window.addEventListener("load", callbackFunc);
      window.addEventListener("resize", callbackFunc);
      window.addEventListener("scroll", callbackFunc);
    
    })();
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
