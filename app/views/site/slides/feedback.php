<?php
/** @var $feedbackForm \app\models\FeedbackForm */
/** @var $asset \yii\web\AssetBundle */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div id="ocenit" class="row delimiter-line slide choose-us">
    <div class="col-md-12">
        <h2>Оцените наше мероприятие</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-auth form-rounded">
                        <?php $form = ActiveForm::begin([
                            'id' => 'form-reg',
                            'enableClientValidation' => true,
                            'action' => Url::to(['/site/feedback'])
                        ]); ?>

                        <?= $form->field($feedbackForm, 'blok', [
                            'template' => "{label}\n<ul class=\"feedback-mark\">{input}</ul>\n",
                        ])->radioList($feedbackForm->marks(),[
                            'item' => function($index, $label, $name, $checked, $value) {

                                $return = '<li>';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" id="' . $name . $index.'" />';
                                $return .= '<label for="' . $name . $index.'">'.$value.'</label>';
                                $return .= '</li>';

                                return $return;
                            }
                        ]) ?>


                        <?= $form->field($feedbackForm, 'spiker', [
                            'template' => "{label}\n<ul class=\"feedback-mark\">{input}</ul>\n",
                        ])->radioList($feedbackForm->marks(),[
                            'item' => function($index, $label, $name, $checked, $value) {

                                $return = '<li>';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" id="' . $name . $index.'" />';
                                $return .= '<label for="' . $name . $index.'">'.$value.'</label>';
                                $return .= '</li>';

                                return $return;
                            }
                        ]) ?>

                        <?= $form->field($feedbackForm, 'novizna', [
                            'template' => "{label}\n<ul class=\"feedback-mark\">{input}</ul>\n",
                        ])->radioList($feedbackForm->marks(),[
                            'item' => function($index, $label, $name, $checked, $value) {

                                $return = '<li>';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" id="' . $name . $index.'" />';
                                $return .= '<label for="' . $name . $index.'">'.$value.'</label>';
                                $return .= '</li>';

                                return $return;
                            }
                        ]) ?>

                        <?= $form->field($feedbackForm, 'polza', [
                            'template' => "{label}\n<ul class=\"feedback-mark\">{input}</ul>\n",
                        ])->radioList($feedbackForm->marks(),[
                            'item' => function($index, $label, $name, $checked, $value) {

                                $return = '<li>';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" id="' . $name . $index.'" />';
                                $return .= '<label for="' . $name . $index.'">'.$value.'</label>';
                                $return .= '</li>';

                                return $return;
                            }
                        ]) ?>

                        <?= $form->field($feedbackForm, 'atmo', [
                            'template' => "{label}\n<ul class=\"feedback-mark\">{input}</ul>\n",
                        ])->radioList($feedbackForm->marks(),[
                            'item' => function($index, $label, $name, $checked, $value) {

                                $return = '<li>';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" id="' . $name . $index.'" />';
                                $return .= '<label for="' . $name . $index.'">'.$value.'</label>';
                                $return .= '</li>';

                                return $return;
                            }
                        ]) ?>

                        <?= $form->field($feedbackForm, 'org', [
                            'template' => "{label}\n<ul class=\"feedback-mark\">{input}</ul>\n",
                        ])->radioList($feedbackForm->marks(),[
                            'item' => function($index, $label, $name, $checked, $value) {

                                $return = '<li>';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" id="' . $name . $index.'" />';
                                $return .= '<label for="' . $name . $index.'">'.$value.'</label>';
                                $return .= '</li>';

                                return $return;
                            }
                        ]) ?>

                        <?= $form->field($feedbackForm, 'isp')->textarea(['rows'=>4]) ?>
                        <?= $form->field($feedbackForm, 'pozh')->textarea(['rows'=>4]) ?>

                        <div class="clearfix"></div>
                        <div class="btn-send">
                            <input type="submit" id="btn-send" value="ОТПРАВИТЬ" class="btn btn-success">
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
