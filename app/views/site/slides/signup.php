<?php
/** @var $signupForm \app\models\SignupForm */
/** @var $asset \yii\web\AssetBundle */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div id="registracia" class="row delimiter-line slide choose-us">
    <div class="col-md-12">
        <h2>Регистрация</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <h2 class="sbercamp-head">SBERCAMP</h2>
                            <p class="text-left">
                                Присоединяйся! Окунись в насыщенную программу обучающих тренингов по интересным
                                тематикам,
                                мастер-классов, проводимых приглашенными спикерами, творческих заданий и, конечно,
                                интерактивных
                                мероприятий.
                            </p>
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <div class="form-auth form-rounded">
                                <?php $form = ActiveForm::begin([
                                    'id' => 'form-reg',
                                    'enableClientValidation' => true,
                                    'action' => Url::to(['/site/signup'])
                                ]); ?>
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <?= $form->field($signupForm, 'fio', [
                                            'template' => "{input}\n{error}",
                                            'inputOptions' => [
                                                'class' => 'form-control input-lg',
                                            ],
                                        ])
                                            ->textInput([
                                                'placeholder' => $signupForm->getAttributeLabel('fio')
                                            ]); ?>
                                        <?= $form->field($signupForm, 'phone', [
                                            'template' => "{input}\n{error}",
                                            'inputOptions' => [
                                                'class' => 'form-control input-lg',
                                            ],
                                        ])
                                            ->textInput([
                                                'placeholder' => $signupForm->getAttributeLabel('phone')
                                            ]); ?>

                                        <?= $form->field($signupForm, 'email', [
                                            'template' => "{input}\n{error}",
                                            'inputOptions' => [
                                                'class' => 'form-control input-lg',
                                            ],
                                        ])
                                            ->textInput([
                                                'placeholder' => $signupForm->getAttributeLabel('email')
                                            ]); ?>

                                        <?= $form->field($signupForm, 'section', [
                                            'template' => "{input}\n{error}",
                                            'inputOptions' => [
                                                'class' => 'form-control input-lg select-img',
                                            ],
                                        ])
                                        ->dropDownList($signupForm->fieldOptions(), [
                                            'prompt' => $signupForm->getAttributeLabel('section'),
                                            'options' => $signupForm->optionSections,
                                        ]); ?>
                                    </div>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="flask">
                                            <img src="<?= $asset->baseUrl ?>/img/flask.png" class="img-responsive" id="flask"/>
                                            <img src="<?= $asset->baseUrl ?>/img/flask-3.png" class="img-responsive" id="flask-3" style="display: none;" />
                                            <img src="<?= $asset->baseUrl ?>/img/flask-4.png" class="img-responsive" id="flask-4" style="display: none;" />
                                            <img src="<?= $asset->baseUrl ?>/img/flask-5.png" class="img-responsive" id="flask-5" style="display: none;" />
                                            <img src="<?= $asset->baseUrl ?>/img/flask-6.png" class="img-responsive" id="flask-6" style="display: none;" />
                                        </div>
                                        <div class="btn-send">
                                            <input type="submit" id="btn-send" value="ОТПРАВИТЬ" class="btn btn-success">
                                        </div>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<<JS
$(document).ready(function(){
    var flasks = $(".flask");
	$(".select-img").change(function() {
	    var key = $(".select-img option:selected").val();
	    if (key != "") {
	        key = "-" + key;
	    }
	    flasks.find("img").hide();
	    $("#flask" + key, flasks).show();
	});
});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
