<div id="section4" class="col-md-12 delimiter-line">
    <div class="choose__us">
        <div>
            <h1>Видеобращение спикеров</h1>
            <div class="col-md-8 col-md-offset-2">
                <?php foreach ($spikery as $spiker): ?>
                    <div class="img-youtube col-md-2">
                        <p><a class='youtube' href="<?= $spiker->data->youtube ?>"><img src="<?=$spiker->thumb(150,100)?>"
                                                                                        class="size-image-video"></a></p>
                        <h4><?=$spiker->title?></h4>
                        <p><?=$spiker->data->desc?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div id="line4" class="line4"></div>
    </div>
</div>
<div id="section2" class="col-md-12">
    <div name="section2" class="choose__us">
        <h1>РАСПИСАНИЕ</h1>
        <div class="col-md-7 col-md-offset-3 shedule-list">
            <?php foreach ($raspisanie as $key => $rasp): ?>
                <a href="#" id="sh<?=$key?>" class="btn btn-success"><?=$rasp->data->date?> <br> <?=$rasp->getTitle()?></a>
            <?php endforeach; ?>
        </div>
        <?php foreach ($raspisanie as $key => $rasp): ?>
            <div class="col-md-6 col-md-offset-4" id="shedule-<?=$key?>">
                <?=$rasp->data->desc?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col-md-12">
        <div id="line4" class="line4"></div>
    </div>
</div>
<div id="bloc-8" class="col-md-12">
    <div class="choose__us">
        <h1>ГАЛЕРЕЯ</h1>
        <div class="col-md-8 col-md-offset-2">
            <?php foreach ($gallery as $item): ?>
                <div class="img-gallery col-md-3">
                    <a href="<?= $item->getImage() ?>" data-lightbox="image"><img src="<?=$item->thumb(250, 150)?>" class="size-image "></a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-md-12">
        <div id="line4" class="line4"></div>
    </div>
</div>
<div id="section22" class="col-md-12">
    <div name="section22" class="choose__us">
        <div class="col-md-12">
            <h1 class="video">Видео с SBERCAMP</h1>
            <!--                <div id="container">-->
            <div class="col-md-6 col-md-offset-3">
                <div id="slides-2" class="text-center">
                    <div class="slides_container">
                        <?php foreach ($video as $vid): ?>
                            <div class="col-md-12 slide">
                                <p><a class='youtube' href="<?=$vid->link?>"><img
                                            src="<?=$vid->thumb(818, 385)?>" class="img-responsive"></a></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <!--                </div>-->
        </div>
    </div>
    <div class="col-md-12">
        <div id="line4" class="line4"></div>
    </div>
</div>