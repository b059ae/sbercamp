<div id="o-proekte" class="row slide choose-us">
    <div class="col-md-12">
        <h2>О проекте</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-10 col-md-9 col-xs-12">
                            <div class="col-md-4 col-xs-12">
                                <p>
                                    <img src="<?= $asset->baseUrl ?>/img/sbercamp.png" class="sbercamp img-responsive"/>
                                </p>
                            </div>
                            <div class="col-md-8 col-xs-12">
                                <p>
                                    SBERCAMP собирает блогеров из разных уголков России. Основные площадки для
                                    публикаций – YouTube и Instagram, LiveJournal, Facebook, Vkontakte.
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>
                                    SBERCAMP –
                                    концентрация знаний о трендах видеоблогинга, медиа-форматов, эффективном контенте, написании
                                    мультимедийных лонгридов и идеальных постов.
                                    Присоединяйся! Тебя ждет насыщенная программа: мастер-класс по нативной рекламе, обучающие
                                    тренинги в потоках по тематикам с участием приглашенных спикеров, творческие задания и, конечно,
                                    интерактивные мероприятия.
                                </p>
                                <p>
                                    Здесь ты узнаешь как подготовить коммерческий контент так, чтобы учесть интересы всех: когда
                                    подписчики в восторге, а заказчик доволен. Что нужно знать о работе с брендом, чтобы крупнейшие
                                    компании страны стремились с тобой сотрудничать.
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                            <h4>При поддержке</h4>
                            <div class="support-img text-center">
                                <img src="<?= $asset->baseUrl ?>/img/hello_b.png" class="img-responsive"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
