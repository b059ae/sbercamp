<?php
/**
 * @var $this \yii\web\View
 * @var $asset \yii\web\AssetBundle
 * @var $raspisanie \yii\easyii\modules\entity\api\ItemObject[]
 */
?>
<div id="raspisanie" class="row delimiter-line slide choose-us">
    <div class="col-md-12">
        <h2>Расписание</h2>
        <div class="main-form container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-12 col-lg-offset-0 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
                            <div class="shedule-menu">
                                <ul id="shedule-tabs" class="nav nav-pills" role="tablist">
                                    <?php foreach ($raspisanie as $key => $rasp) : ?>
                                        <li role="presentation"<?php if ($key == 1) : echo ' class="active"'; endif; ?>>
                                            <a id="sh-<?= $key; ?>" class="btn btn-success" role="tab" data-toggle="tab"
                                               aria-controls="shedule-<?= $key; ?>" href="#shedule-<?= $key; ?>">
                                                <strong><?= $rasp->data->date; ?></strong>
                                                <br> <?= str_replace('|', '<br>', $rasp->getTitle()); ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="tab-content">
                                <?php foreach ($raspisanie as $key => $rasp) : ?>
                                    <div id="shedule-<?= $key; ?>" role="tabpanel"
                                         class="shedule-table tab-pane<?php if ($key == 1) : echo ' active'; endif; ?>">
                                        <?= $rasp->data->desc; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
