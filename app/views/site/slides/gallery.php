<?php
/** @var $gallery \yii\easyii\modules\gallery\api\PhotoObject[] */

?>

<div id="galereya" class="row delimiter-line slide choose-us">
    <div class="col-md-12">
        <h2>Галерея</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="clearfix mosaicflow" data-min-item-width="300">
                                <?php foreach (array_slice($gallery, 0, 9) as $item): ?>
                                    <div class="mosaicflow__item">
                                        <a href="<?= $item->getImage() ?>" class="photo">
                                            <img src="<?= $item->getImage() ?>" class="img-thumbnail">
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                                <?php
                                //Просмотр всех фоток в слайдшоу
                                ?>
                                <?php foreach (array_slice($gallery, 9) as $item): ?>
                                    <a href="<?= $item->getImage() ?>" class="photo" style="display: none;">
                                        <img src="<?= $item->getImage() ?>" class="img-thumbnail">
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>