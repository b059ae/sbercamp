<?php
/**
 * @var $this \yii\web\View
 * @var $asset \yii\web\AssetBundle
 * @var $video \yii\easyii\modules\carousel\api\CarouselObject[]
 */
?>
<div id="slide-video" class="row slide choose-us">
    <div class="col-md-12">
        <h2>Видео с SBERCAMP</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="carousel-roundabout" class="text-center">
                                <ul id="slides-container" class="hidden-xs hidden-sm">
                                    <?php foreach ($video as $vid): ?>
                                        <li>
                                            <a class="youtube" href="<?= $vid->link ?>">
                                                <span class="video-youtube"></span>
                                                <img src="<?= $vid->thumb(818, 385); ?>" class="img-responsive">
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <ul id="slides-xs" class="list-unstyled visible-xs visible-sm">
                                    <?php foreach ($video as $vid): ?>
                                        <li>
                                            <a class="youtube" href="<?= $vid->link ?>">
                                                <span class="video-youtube"></span>
                                                <img src="<?= $vid->thumb(320, 150); ?>" class="img-responsive center-block">
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
