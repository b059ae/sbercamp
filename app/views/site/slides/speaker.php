<?php
/** @var $spikery \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var $asset \yii\web\AssetBundle */

?>
<div id="spikery" class="row delimiter-line slide choose-us">
    <div class="col-md-12">
        <h2>Видеобращение спикеров</h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                    <?php foreach ($spikery as $i => $spiker): ?>
                        <div class="col-md-3<?=($i==0 && count($spikery)<3)?' col-md-offset-3':''?>">
                            <?php if (strlen($spiker->data->youtube)>0):?>
                            <a href="<?=$spiker->data->youtube?>" class="youtube">
                            <?php endif; ?>
                                <div class="wrapper-rounded-avatar center-block ch-item">
                                    <div class="rounded-avatar">
                                        <img src="<?=$spiker->thumb(130, 130)?>">
                                    </div>
                                    <?php if (strlen($spiker->data->youtube)>0):?>
                                        <div class="ch-info">
                                            <img src="<?= $asset->baseUrl ?>/img/play.png" class="play-img">
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="speaker-text">
                                    <h4><?=$spiker->title?></h4>
                                    <p><?=$spiker->data->desc?></p>
                                </div>
                            <?php if (strlen($spiker->data->youtube)>0):?>
                            </a>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>