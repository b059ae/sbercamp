<?php
/** @var $page \yii\easyii\modules\page\api\PageObject */
/** @var $ucastniki \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var $gallery \yii\easyii\modules\gallery\api\PhotoObject[] */

use app\helpers\Html;
$asset = \app\assets\AppAsset::register($this);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title');
?>
<div id="raspisanie" class="row delimiter-line slide choose-us">
    <div class="col-md-12">
        <div id="fixed-menu">
            <div class="main-form container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="shedule-menu uchastniki-menu">
                            <ul id="shedule-tabs" class="nav nav-pills">
                                <li>
                                    <a id="slide-instagram-blogging" class="btn btn-success background-color-3" href="#instagram-blogging" >
                                        <strong>Instagram blogging</strong>
                                    </a>
                                </li>
                                <li>
                                    <a id="slide-youtube-vlogging" class="btn btn-success background-color-4" href="#youtube-vlogging" >
                                        <strong>YouTube vlogging</strong>
                                    </a>
                                </li>
                                <li>
                                    <a id="slide-multimediynie-longridy" class="btn btn-success longed background-color-5" href="#multimediynie-longridy" >
                                        <strong>Мультимедийные лонгриды</strong>
                                    </a>
                                </li>
                                <li>
                                    <a id="slide-facebookvkontakte" class="btn btn-success background-color-6" href="#facebookvkontakte" >
                                        <strong>Facebook vkontakte</strong>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($ucastniki as $cat) : ?>

<div id="<?= $cat->slug ?>" class="row delimiter-line slide uchastniki choose-us section-color-<?=$cat->getId()?>">
    <div class="col-md-12">
        <h2><?= $cat->title; ?></h2>
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">

                    <?php
                    $countInRow = 5;
                    $countCeil = 9;
                    $items = $cat->getItems();
                    for ($i = 0; $i < $countCeil; $i++) :
                        /*if (($i % $countInRow) == 0) {
                            ?><div class="row"><?php
                        }*/
                        if (isset($items[$i])) : ?>
                            <div class="col-sm-6 col-md-2<?php if (($i % $countInRow) == 0) : echo ' col-md-offset-1 col-sm-offset-0'; endif; ?>">
                                <a href="<?= $items[$i]->data->link; ?>" target="_blank">
                                    <div class="wrapper-rounded-avatar center-block ch-item">
                                        <div class="rounded-avatar">
                                            <img src="<?=$items[$i]->thumb(130, 130)?>">
                                        </div>
                                        <?php if (strlen($items[$i]->data->link)>0):?>
                                            <div class="ch-info">
                                                <img src="<?= $asset->baseUrl ?>/img/hand.png" class="play-img">
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="speaker-text">
                                        <h4><?= $items[$i]->getTitle(); ?></h4>
                                    </div>
                                </a>
                            </div>
                        <?php else : ?>
                            <div class="col-sm-6 col-md-2<?php if (($i % $countInRow) == 0) : echo ' col-md-offset-1 col-sm-offset-0'; endif; ?>">
                                <div class="wrapper-rounded-avatar center-block">
                                    <div class="rounded-avatar">
                                        <img src="<?= $asset->baseUrl ?>/img/waiter.jpg">
                                    </div>
                                </div>
                                <div class="speaker-text">
                                    <h4>&nbsp;</h4>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php /* $j = $i + 1; if (($j % $countInRow) == 0 || $j == $countCeil) {
                            ?></div><?php
                        }*/ ?>
                    <?php endfor; ?>
                    </div>
                    <div class="row uchastniki-galereya">
                        <h2>Галерея работ</h2>
                        <div id="carousel-roundabout" class="text-center">
                            <ul id="slides-container" class="gallery-container hidden-xs hidden-sm">
                                <?php foreach ($gallery[$cat->slug] as $image): ?>
                                    <li>
                                        <a class="photo" href="<?= $image->getImage() ?>" title="<p><?=Html::replaceLink($image->description) ?></p>">
                                            <img src="<?= $image->thumb(818, 385); ?>" class="img-responsive">
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <ul id="slides-xs" class="list-unstyled visible-xs visible-sm">
                                <?php foreach ($gallery[$cat->slug] as $image): ?>
                                    <li>
                                        <p><?=Html::replaceLink($image->description) ?></p>
                                        <img src="<?= $image->thumb(720, 340); ?>" class="img-responsive center-block">
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
<style>
    /*uchastniki colorbox*/
    #cboxTitle{
        background-color: white;
        width: 100%;
        top: -33px;
        height: 33px;
        padding-left: 10px;
    }
    #cboxTitle a, #cboxTitle p{
        font-size: 14px;
    }
    #cboxTitle p{
        width: 90%;
    }
</style>
<?php
$script = <<<JS
    var header = $("#slide-main");
    var headMenu = $("#top");
    var fixedMenu = $("#fixed-menu");
    var offset = header.offset().top + header.height();
    var scrollOffset = headMenu.height() + fixedMenu.height();
    fixedMenu.parent().height(fixedMenu.height());

    function scroll() {
        var top = $(window).scrollTop();
        if (top >= offset) {
            if (!fixedMenu.hasClass("affix")) {
                fixedMenu.addClass("affix").css({
                    "top": headMenu.height(),
                    "width": $(window).width()
                });
            }
        } else {
            fixedMenu.removeClass("affix").removeAttr("style");
        }
    }
    document.onscroll = scroll;
    scroll();

    $('#shedule-tabs a').click(function(e) {
        e.preventDefault();
        var scrollTo = $(this).attr('href');
        if ($(scrollTo).length != 0) {
            $("html, body").animate({
                scrollTop: $(scrollTo).offset().top - scrollOffset
            }, 500);
        }
    });
    
    $('.gallery-container').roundabout({
        minScale: 0.8,
        childSelector: "li",
        autoplay: false,
        /*duration:1000,
        autoplayDuration:4000,
        autoplayPauseOnHover: true,*/
        dots: true
    });
    
    $(".photo").colorbox({maxWidth:"80%", maxHeight:"80%", rel:"photo-slide", transition:"fade"});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

