-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `easyii_admins`;
CREATE TABLE `easyii_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `auth_key` varchar(128) DEFAULT NULL,
  `access_token` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_token` (`access_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_article_categories`;
CREATE TABLE `easyii_article_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_article_items`;
CREATE TABLE `easyii_article_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_carousel`;
CREATE TABLE `easyii_carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file` varchar(128) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_carousel` (`id`, `image_file`, `link`, `title`, `text`, `order_num`, `status`) VALUES
(1,	'carousel/sbercamp1-57cd30d908.png',	'https://www.youtube.com/watch?v=Ky9Ac0bilJU',	'',	'',	1,	1),
(2,	'carousel/sbercamp2-55c88d9c53.png',	'https://www.youtube.com/watch?v=Ky9Ac0bilJU',	'',	'',	2,	1),
(3,	'carousel/sbercamp3-ce1b4e4c27.png',	'https://www.youtube.com/watch?v=Ky9Ac0bilJU',	'',	'',	3,	1);

DROP TABLE IF EXISTS `easyii_catalog_categories`;
CREATE TABLE `easyii_catalog_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_catalog_categories` (`id`, `title`, `description`, `image_file`, `fields`, `slug`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(1,	'Спикеры',	NULL,	NULL,	'[{\"name\":\"desc\",\"title\":\"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"youtube\",\"title\":\"\\u0412\\u0438\\u0434\\u0435\\u043e youtube\",\"type\":\"string\",\"options\":\"\"}]',	'spikery',	1,	1,	2,	0,	1,	1),
(2,	'Участники',	NULL,	NULL,	'{}',	'ucastniki',	2,	1,	10,	0,	2,	1),
(3,	'Instagram blogging',	NULL,	NULL,	'[{\"name\":\"link\",\"title\":\"\\u0421\\u0441\\u044b\\u043b\\u043a\\u0430 \\u043d\\u0430 \\u0431\\u043b\\u043e\\u0433\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"email\",\"title\":\"Email\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"phone\",\"title\":\"\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\",\"type\":\"string\",\"options\":\"\"}]',	'instagram-blogging',	2,	2,	3,	1,	2,	1),
(4,	'YouTube vlogging',	NULL,	NULL,	'[{\"name\":\"link\",\"title\":\"\\u0421\\u0441\\u044b\\u043b\\u043a\\u0430 \\u043d\\u0430 \\u0431\\u043b\\u043e\\u0433\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"email\",\"title\":\"Email\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"phone\",\"title\":\"\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\",\"type\":\"string\",\"options\":\"\"}]',	'youtube-vlogging',	2,	4,	5,	1,	2,	1),
(5,	'Мультимедийные лонгриды',	NULL,	NULL,	'[{\"name\":\"link\",\"title\":\"\\u0421\\u0441\\u044b\\u043b\\u043a\\u0430 \\u043d\\u0430 \\u0431\\u043b\\u043e\\u0433\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"email\",\"title\":\"Email\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"phone\",\"title\":\"\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\",\"type\":\"string\",\"options\":\"\"}]',	'multimediynie-longridy',	2,	6,	7,	1,	2,	1),
(6,	'Facebook / vkontakte: трендсеттеры и лидеры мнений',	NULL,	NULL,	'[{\"name\":\"link\",\"title\":\"\\u0421\\u0441\\u044b\\u043b\\u043a\\u0430 \\u043d\\u0430 \\u0431\\u043b\\u043e\\u0433\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"email\",\"title\":\"Email\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"phone\",\"title\":\"\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\",\"type\":\"string\",\"options\":\"\"}]',	'facebookvkontakte',	2,	8,	9,	1,	2,	1);

DROP TABLE IF EXISTS `easyii_catalog_items`;
CREATE TABLE `easyii_catalog_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `available` int(11) DEFAULT '1',
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  `data` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_catalog_items` (`id`, `category_id`, `title`, `description`, `available`, `price`, `discount`, `data`, `image_file`, `slug`, `time`, `status`) VALUES
(33,	5,	'Станислав Самойлик',	'<p>Ростов-на-Дону</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/pierwszy.livejournal.com\\/\",\"email\":\"\",\"phone\":\"\"}',	'catalog/a1ff05a173.png',	'-8',	1489669088,	1),
(34,	6,	'Ольга Смотрина',	'',	1,	0,	0,	'{\"link\":\"http:\\/\\/instagram.com\\/smotrina\",\"email\":\"\",\"phone\":\"\"}',	'catalog/5a37a7c32f.png',	'-9',	1489733118,	1),
(31,	6,	'Василий Никитинский',	'<p>Владимир</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/dobriy-vasya.livejournal.com\\/ \",\"email\":\"\",\"phone\":\"\"}',	'catalog/666ff8a6c6.png',	NULL,	1489740816,	1),
(49,	6,	'Илья Захаров',	'',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/z.ilya\",\"email\":\"\",\"phone\":\"\"}',	'catalog/b1ffa15863.jpg',	NULL,	1489999460,	1),
(30,	3,	'Арти Индиго',	'<p>Хабаровск </p>',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/artee_indigo\",\"email\":\"\",\"phone\":\"\"}',	'catalog/fe3f26701f.png',	'-6',	1489658922,	1),
(28,	6,	'Мусса Бекмурзиев',	'<p>Магас</p>',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/skala06\",\"email\":\"\",\"phone\":\"\"}',	'catalog/ac2cdf74e9.png',	'-4',	1489657376,	1),
(29,	3,	'Анна Мамаева',	'<p>Иркутск</p>',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/annamamaeva38\\/ \",\"email\":\"\",\"phone\":\"\"}',	'catalog/1ca4f24d09.png',	'-5',	1489657703,	0),
(26,	4,	'Амар Жужуев',	'<p>Черкесск</p>',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/amar_zhu\",\"email\":\"mirotvorec1@yandex.ru\",\"phone\":\"\"}',	'catalog/3bb4ad8193.png',	'-2',	1489671349,	1),
(27,	6,	'Муслим Алимирзаев',	'<p>Пятигорск</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/instagram.com\\/muslim051975 \",\"email\":\"\",\"phone\":\"\"}',	'catalog/ee46ce644e.png',	NULL,	1489673641,	1),
(24,	4,	'Шамиль Амиров',	'<p>Каспийск</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/instagram.com\\/amishami \",\"email\":\"\",\"phone\":\"\"}',	'catalog/b50f2f3d4b.png',	'-22',	1489736010,	1),
(25,	3,	'Кристина Мартьянова',	'<p>Краснодар </p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/instagram.com\\/kris__mart \",\"email\":\"redmilk11@gmail.com\",\"phone\":\"\"}',	'catalog/1342870ca8.png',	NULL,	1489672778,	1),
(7,	1,	'Таня Иванова',	'',	1,	NULL,	NULL,	'{\"desc\":\"\\u041a\\u0443\\u0440\\u0430\\u0442\\u043e\\u0440 \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430 \\u00abYouTube vlogging\\u00bb\",\"youtube\":\"https:\\/\\/www.youtube.com\\/embed\\/b-78_0SkI0Q\"}',	'catalog/b132057cc1.jpg',	'-12',	1489732545,	1),
(8,	1,	'Сергей Король',	'',	1,	NULL,	NULL,	'{\"desc\":\"\\u041a\\u0443\\u0440\\u0430\\u0442\\u043e\\u0440 \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430 \\u00ab\\u041c\\u0443\\u043b\\u044c\\u0442\\u0438\\u043c\\u0435\\u0434\\u0438\\u0439\\u043d\\u044b\\u0435 \\u043b\\u043e\\u043d\\u0433\\u0440\\u0438\\u0434\\u044b\\u00bb \",\"youtube\":\"https:\\/\\/www.youtube.com\\/embed\\/Ui09Ybq5t5g \"}',	'catalog/518ed94327.jpg',	'-11',	1489678497,	1),
(15,	3,	'Александра Савичева',	'<p>Ростов-на-Дону</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/ajlis.livejournal.com\",\"email\":\"asavicheva@gmail.com\",\"phone\":\"\"}',	'catalog/5c480d214d.png',	'-3',	1489672064,	1),
(14,	4,	'Наташа Веретельник',	'<p>Ростов-на-Дону</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/risova.livejournal.com\\/\",\"email\":\"natalia.veretelnik@gmail.com\",\"phone\":\"\"}',	'catalog/1437a2460e.png',	'-23',	1489658786,	1),
(13,	6,	'Яков Осканов',	'<p>Ростов-на-Дону</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/oskanov.livejournal.com\\/\",\"email\":\"oskanov@inbox.ru\",\"phone\":\"\"}',	'catalog/07358c1846.png',	NULL,	1489657207,	1),
(16,	3,	'Дмитрий Парий',	'<p>Краснодар</p>',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/pariy.dima\\/ \",\"email\":\"pariy@bk.ru\",\"phone\":\"\"}',	'catalog/d8e8130784.png',	NULL,	1489679055,	1),
(17,	3,	'Елена Каск',	'<p>Краснодар</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/fluger19.livejournal.com\\/ \",\"email\":\"fluger_po_vetru@mail.ru\",\"phone\":\"\"}',	'catalog/6cdb6eb21a.png',	NULL,	1489674577,	1),
(18,	3,	'Ибрагим Заурбеков',	'<p>Грозный</p>',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/beno_men\\/\",\"email\":\"ibrazaur@yahoo.com\",\"phone\":\"\"}',	'catalog/25ea5dc3a9.png',	NULL,	1489673326,	1),
(19,	3,	'Ризван Эдильсултанов',	'<p>Грозный</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/instagram.com\\/rizvan82 \",\"email\":\"\",\"phone\":\"\"}',	'catalog/6c6751054e.png',	NULL,	1489674428,	1),
(20,	6,	'Эрдни Стоянов',	'<p>Элиста</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/instagram.com\\/erdny\",\"email\":\"logopro.elista@gmail.com\",\"phone\":\"\"}',	'catalog/007e93f1a6.png',	NULL,	1489672143,	1),
(21,	3,	'Оксана Кочкарова',	'<p>Черкесск</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/instagram.com\\/okochkarova\",\"email\":\"okochkarova@mail.ru\",\"phone\":\"\"}',	'catalog/1519d1067f.png',	NULL,	1489672947,	1),
(22,	5,	'Алим Калибатов',	'<p>Нальчик</p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/radioman07.livejournal.com\",\"email\":\"tino232@list.ru\",\"phone\":\"\"}',	'catalog/4c259499dd.png',	'-20',	1489658591,	1),
(23,	5,	'Сергей Евсюков',	'<p>Краснодар </p>',	1,	0,	0,	'{\"link\":\"http:\\/\\/www.krdblog.com\\/\",\"email\":\"oaklandp3@gmail.com\",\"phone\":\"\"}',	'catalog/29a333ad87.png',	'-19',	1489736124,	1),
(35,	1,	'Александр Якунин',	'',	1,	0,	0,	'{\"desc\":\"\\u041a\\u0443\\u0440\\u0430\\u0442\\u043e\\u0440 \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430                                \\u00abInstagram blogging\\u00bb \",\"youtube\":\"https:\\/\\/www.youtube.com\\/embed\\/VY9SZ4hyRzY\"}',	'catalog/226c8f1d15.png',	'-13',	1489677194,	1),
(36,	1,	'Павел Перец',	'',	1,	0,	0,	'{\"desc\":\"\\u041d\\u0430\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f \\u0440\\u0435\\u043a\\u043b\\u0430\\u043c\\u0430 \\u0432 \\u043c\\u0435\\u0434\\u0438\\u0430\",\"youtube\":\"https:\\/\\/www.youtube.com\\/embed\\/eVdtXxz45Z8\"}',	'catalog/6a41abc5d7.png',	'-14',	1489515735,	1),
(37,	3,	'Ольга Гнатко',	'<p>Ярославль</p>',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/slavolka\\/\",\"email\":\"\",\"phone\":\"\"}',	'catalog/75df34555c.png',	NULL,	1489676698,	1),
(38,	1,	'Сергей Ешанов',	'',	1,	0,	0,	'{\"desc\":\"\\u041a\\u0443\\u0440\\u0430\\u0442\\u043e\\u0440 \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430 \\u00abFacebook\\/vkontakte: \\u0442\\u0440\\u0435\\u043d\\u0434\\u0441\\u0435\\u0442\\u0442\\u0435\\u0440\\u044b \\u0438 \\u043b\\u0438\\u0434\\u0435\\u0440\\u044b \\u043c\\u043d\\u0435\\u043d\\u0438\\u0439\\u00bb\",\"youtube\":\"https:\\/\\/www.youtube.com\\/\\/embed\\/Zo6oKCiTktU\"}',	'catalog/e2de2d672e.png',	'-10',	1489515718,	1),
(39,	1,	'Василий Ящук',	'',	1,	0,	0,	'{\"desc\":\"\\\"YouTube vlogging\\\"\",\"youtube\":\"https:\\/\\/www.youtube.com\\/embed\\/cxXj3EOsaSo\"}',	'catalog/3711d21a5b.png',	'-15',	1489731722,	0),
(40,	4,	'Аня Мамаева',	'',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.instagram.com\\/annamamaeva38\\/ \",\"email\":\"\",\"phone\":\"\"}',	'catalog/f217d259d4.png',	'-16',	1489736445,	1),
(41,	4,	'Руслан Аларханов',	'',	1,	0,	0,	'{\"link\":\"https:\\/\\/m.youtube.com\\/user\\/Bpa4o\",\"email\":\"\",\"phone\":\"\"}',	'catalog/bfa1024e16.jpg',	'-17',	1489737971,	1),
(42,	5,	'Анатолий Тверитнев',	'',	1,	0,	0,	'{\"link\":\"https:\\/\\/vk.com\\/tveritnev_rules \",\"email\":\"\",\"phone\":\"\"}',	'catalog/7057f1eeb1.png',	'-18',	1489658425,	1),
(43,	4,	'Денис Суворов',	'',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.facebook.com\\/superday77?fref=ts\",\"email\":\"\",\"phone\":\"\"}',	'catalog/b5f3d26846.png',	'-21',	1489657006,	1),
(44,	4,	'Алексей Соломатин',	'',	1,	0,	0,	'{\"link\":\"http:\\/\\/solomatin.livejournal.com\\/\",\"email\":\"\",\"phone\":\"\"}',	'catalog/4ebf187f93.png',	'-24',	1489739936,	1),
(45,	4,	'Михаил Малышев',	'',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.facebook.com\\/mikhail.malyshev\",\"email\":\"\",\"phone\":\"\"}',	'catalog/2df2b7ebda.png',	'-25',	1490080094,	1),
(46,	6,	'Андрей Долженков',	'',	1,	0,	0,	'{\"link\":\"https:\\/\\/www.facebook.com\\/dolgak\",\"email\":\"\",\"phone\":\"\"}',	'catalog/21b0ac9c4c.jpg',	'-26',	1489655646,	1),
(47,	3,	'Резерв',	'',	1,	0,	0,	'{\"link\":\"\",\"email\":\"\",\"phone\":\"\"}',	NULL,	NULL,	1489743768,	0),
(48,	3,	'Тест',	'',	1,	0,	0,	'{\"link\":\"\",\"email\":\"\",\"phone\":\"\"}',	NULL,	NULL,	1489743813,	0),
(50,	4,	'Руслан Аларханов',	'',	1,	0,	0,	'{\"link\":\"\",\"email\":\"\",\"phone\":\"\"}',	NULL,	'-7',	1489656512,	0),
(51,	4,	'Виктор Шаров',	'',	1,	0,	0,	'{\"link\":\"\",\"email\":\"\",\"phone\":\"\"}',	'catalog/8e856378b5.png',	'',	1490080409,	1);

DROP TABLE IF EXISTS `easyii_catalog_item_data`;
CREATE TABLE `easyii_catalog_item_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id_name` (`item_id`,`name`),
  KEY `value` (`value`(300))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_catalog_item_data` (`id`, `item_id`, `name`, `value`) VALUES
(1029,	31,	'phone',	''),
(807,	29,	'phone',	''),
(806,	29,	'email',	''),
(1047,	28,	'phone',	''),
(1046,	28,	'email',	''),
(1035,	27,	'phone',	''),
(1045,	28,	'link',	'https://www.instagram.com/skala06'),
(1034,	27,	'email',	''),
(1027,	31,	'link',	'http://dobriy-vasya.livejournal.com/ '),
(1033,	27,	'link',	'http://instagram.com/muslim051975 '),
(1177,	26,	'email',	'mirotvorec1@yandex.ru'),
(1178,	26,	'phone',	''),
(1065,	13,	'phone',	''),
(1176,	26,	'link',	'https://www.instagram.com/amar_zhu'),
(1171,	24,	'email',	''),
(1172,	24,	'phone',	''),
(63,	9,	'email',	'435543@dfsafd.hh'),
(64,	10,	'phone',	'89281754466'),
(68,	12,	'phone',	'213123123123'),
(1064,	13,	'email',	'oskanov@inbox.ru'),
(1183,	14,	'email',	'natalia.veretelnik@gmail.com'),
(62,	9,	'phone',	'89281754466'),
(67,	11,	'email',	'435543@dfsafd.hh'),
(66,	11,	'phone',	'89281754466'),
(623,	19,	'email',	''),
(1170,	24,	'link',	'http://instagram.com/amishami '),
(788,	25,	'email',	'redmilk11@gmail.com'),
(65,	10,	'email',	'435543@dfsafd.hh'),
(69,	12,	'email',	'435543@dfsafd.hh'),
(1063,	13,	'link',	'http://oskanov.livejournal.com/'),
(1184,	14,	'phone',	''),
(1116,	46,	'phone',	''),
(836,	15,	'email',	'asavicheva@gmail.com'),
(622,	19,	'link',	'http://instagram.com/rizvan82 '),
(875,	8,	'youtube',	'https://www.youtube.com/embed/Ui09Ybq5t5g '),
(1210,	7,	'youtube',	'https://www.youtube.com/embed/b-78_0SkI0Q'),
(1209,	7,	'desc',	'Куратор потока «YouTube vlogging»'),
(787,	25,	'link',	'http://instagram.com/kris__mart '),
(835,	15,	'link',	'http://ajlis.livejournal.com'),
(530,	17,	'email',	'fluger_po_vetru@mail.ru'),
(1124,	30,	'email',	''),
(837,	15,	'phone',	''),
(529,	17,	'link',	'http://fluger19.livejournal.com/ '),
(743,	18,	'email',	'ibrazaur@yahoo.com'),
(1123,	30,	'link',	'https://www.instagram.com/artee_indigo'),
(1039,	20,	'link',	'http://instagram.com/erdny'),
(738,	21,	'phone',	''),
(952,	22,	'link',	'http://radioman07.livejournal.com'),
(1061,	23,	'email',	'oaklandp3@gmail.com'),
(742,	18,	'link',	'https://www.instagram.com/beno_men/'),
(531,	17,	'phone',	''),
(1041,	20,	'phone',	''),
(737,	21,	'email',	'okochkarova@mail.ru'),
(954,	22,	'phone',	''),
(1060,	23,	'link',	'http://www.krdblog.com/'),
(1040,	20,	'email',	'logopro.elista@gmail.com'),
(736,	21,	'link',	'http://instagram.com/okochkarova'),
(624,	19,	'phone',	''),
(953,	22,	'email',	'tino232@list.ru'),
(1062,	23,	'phone',	''),
(1028,	31,	'email',	''),
(1142,	39,	'youtube',	'https://www.youtube.com/embed/cxXj3EOsaSo'),
(1137,	33,	'email',	''),
(1100,	34,	'email',	''),
(1092,	35,	'youtube',	'https://www.youtube.com/embed/VY9SZ4hyRzY'),
(874,	8,	'desc',	'Куратор потока «Мультимедийные лонгриды» '),
(1140,	36,	'youtube',	'https://www.youtube.com/embed/eVdtXxz45Z8'),
(1130,	49,	'link',	'https://www.instagram.com/z.ilya'),
(479,	37,	'email',	''),
(480,	37,	'phone',	''),
(478,	37,	'link',	'https://www.instagram.com/slavolka/'),
(744,	18,	'phone',	''),
(1136,	33,	'link',	'http://pierwszy.livejournal.com/'),
(1125,	30,	'phone',	''),
(789,	25,	'phone',	''),
(483,	16,	'phone',	''),
(805,	29,	'link',	'https://www.instagram.com/annamamaeva38/ '),
(1138,	33,	'phone',	''),
(482,	16,	'email',	'pariy@bk.ru'),
(1099,	34,	'link',	'http://instagram.com/smotrina'),
(481,	16,	'link',	'https://www.instagram.com/pariy.dima/ '),
(1101,	34,	'phone',	''),
(1165,	40,	'email',	''),
(896,	38,	'desc',	'Куратор потока «Facebook/vkontakte: трендсеттеры и лидеры мнений»'),
(897,	38,	'youtube',	'https://www.youtube.com//embed/Zo6oKCiTktU'),
(1141,	39,	'desc',	'\"YouTube vlogging\"'),
(1139,	36,	'desc',	'Нативная реклама в медиа'),
(1021,	42,	'link',	'https://vk.com/tveritnev_rules '),
(1022,	42,	'email',	''),
(1166,	40,	'phone',	''),
(1164,	40,	'link',	'https://www.instagram.com/annamamaeva38/ '),
(1189,	43,	'email',	''),
(1023,	42,	'phone',	''),
(1114,	46,	'link',	'https://www.facebook.com/dolgak'),
(1115,	46,	'email',	''),
(1182,	14,	'link',	'http://risova.livejournal.com/'),
(1146,	45,	'link',	'https://www.facebook.com/mikhail.malyshev'),
(1152,	44,	'link',	'http://solomatin.livejournal.com/'),
(1190,	43,	'phone',	''),
(1188,	43,	'link',	'https://www.facebook.com/superday77?fref=ts'),
(1079,	47,	'email',	''),
(1078,	47,	'link',	''),
(1080,	47,	'phone',	''),
(1085,	48,	'email',	''),
(1084,	48,	'link',	''),
(1086,	48,	'phone',	''),
(1091,	35,	'desc',	'Куратор потока                                «Instagram blogging» '),
(1131,	49,	'email',	''),
(1132,	49,	'phone',	''),
(1200,	50,	'link',	''),
(1147,	45,	'email',	''),
(1148,	45,	'phone',	''),
(1201,	50,	'email',	''),
(1153,	44,	'email',	''),
(1154,	44,	'phone',	''),
(1207,	41,	'email',	''),
(1202,	50,	'phone',	''),
(1198,	51,	'email',	''),
(1197,	51,	'link',	''),
(1199,	51,	'phone',	''),
(1208,	41,	'phone',	''),
(1206,	41,	'link',	'https://m.youtube.com/user/Bpa4o');

DROP TABLE IF EXISTS `easyii_entity_categories`;
CREATE TABLE `easyii_entity_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `cache` tinyint(1) NOT NULL DEFAULT '1',
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_entity_categories` (`id`, `title`, `description`, `image_file`, `fields`, `slug`, `cache`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(1,	'Расписание',	'',	NULL,	'[{\"name\":\"date\",\"title\":\"\\u0414\\u0430\\u0442\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"desc\",\"title\":\"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"type\":\"html\",\"options\":\"\"}]',	'raspisanie',	1,	1,	1,	2,	0,	1,	1);

DROP TABLE IF EXISTS `easyii_entity_items`;
CREATE TABLE `easyii_entity_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `data` text NOT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_entity_items` (`id`, `category_id`, `title`, `data`, `order_num`, `status`) VALUES
(1,	1,	'День знакомства | Гранд Отель',	'{\"date\":\"22.03.2017\",\"desc\":\"<table>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<td>19:00 - 22:00<\\/td><td>\\u0423\\u0436\\u0438\\u043d, \\u043e\\u0431\\u0449\\u0430\\u044f \\u0432\\u0441\\u0442\\u0440\\u0435\\u0447\\u0430, \\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0441\\u0442\\u0432\\u043e<\\/td>\\r\\n<\\/tr>\\r\\n\\r\\n<\\/tbody>\\r\\n<\\/table><p><br>\\r\\n<\\/p><p>\\u041c\\u043e\\u0434\\u0435\\u0440\\u0430\\u0442\\u043e\\u0440\\u044b: <em>\\u041a\\u0430\\u0440\\u0438\\u043d\\u0430 \\u041a\\u043e\\u0440\\u044f\\u043a\\u043e\\u0432\\u0446\\u0435\\u0432\\u0430, \\u0422\\u0430\\u0442\\u044c\\u044f\\u043d\\u0430 \\u041c\\u0430\\u0433\\u0435\\u0440\\u0430\\r\\n<\\/em><\\/p>\"}',	1,	1),
(2,	1,	'Образовательный день | Гранд Отель',	'{\"date\":\"23.03.2017\",\"desc\":\"<table>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<td>08:00 - 08:15\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0417\\u0430\\u0440\\u044f\\u0434\\u043a\\u0430\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>08:15 - 09:15\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0417\\u0430\\u0432\\u0442\\u0440\\u0430\\u043a\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>09:30 - 10:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0420\\u0435\\u0433\\u0438\\u0441\\u0442\\u0440\\u0430\\u0446\\u0438\\u044f\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>10:00 - 10:30\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041e\\u0442\\u043a\\u0440\\u044b\\u0442\\u0438\\u0435 SBERCAMP \\u2013 \\u0432\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f <em>\\u041a\\u043e\\u0440\\u044f\\u043a\\u043e\\u0432\\u0446\\u0435\\u0432\\u0430 \\u041a\\u0430\\u0440\\u0438\\u043d\\u0430<br><\\/em><ul><li><strong><i>\\u041f\\u0440\\u0438\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0438\\u0435\\r\\n\\u0423\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0435\\u0433\\u043e \\u0421\\u0442\\u0430\\u0432\\u0440\\u043e\\u043f\\u043e\\u043b\\u044c\\u0441\\u043a\\u043e\\u0433\\u043e \\u043e\\u0442\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f \\u042e\\u0417\\u0411 \\u2013 <\\/i><i>\\u0420.\\u0412. \\u0420\\u043e\\u043c\\u0430\\u043d\\u0435\\u043d\\u043a\\u043e<\\/i><\\/strong><\\/li><li><strong><i>\\u041f\\u0440\\u0438\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0438\\u0435\\r\\n\\u0422\\u0430\\u0442\\u044c\\u044f\\u043d\\u044b \\u041c\\u0430\\u0433\\u0435\\u0440\\u044b<\\/i><\\/strong><\\/li><li><i><strong>\\u041f\\u0440\\u0438\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0438\\u0435 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432\\r\\n\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430<\\/strong><\\/i><\\/li><\\/ul>\\r\\n\\r\\n<strong><\\/strong><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>10:30 - 12:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041b\\u0435\\u043a\\u0446\\u0438\\u0438:<ul><li><strong><i>\\u041d\\u0430\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f \\u0440\\u0435\\u043a\\u043b\\u0430\\u043c\\u0430\\r\\n\\u0432 \\u043c\\u0435\\u0434\\u0438\\u0430. \\u041f\\u0430\\u0432\\u0435\\u043b \\u041f\\u0435\\u0440\\u0435\\u0446.<\\/i><\\/strong><\\/li><li><strong><i><\\/i><\\/strong><strong><i>\\u041a\\u0430\\u043a \\u0438\\u0437\\u043c\\u0435\\u0440\\u044f\\u0442\\u044c \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0438\\u0432\\u0430\\u0442\\u044c \\u044d\\u0444\\u0444\\u0435\\u043a\\u0442\\u0438\\u0432\\u043d\\u043e\\u0441\\u0442\\u044c \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439 \\u0432 \\u0441\\u043e\\u0446\\u0441\\u0435\\u0442\\u044f\\u0445. \\u0410\\u043b\\u0435\\u043a\\u0441\\u0430\\u043d\\u0434\\u0440 \\u042f\\u043a\\u0443\\u043d\\u0438\\u043d.<\\/i><\\/strong><\\/li>\\r\\n\\t\\t<\\/ul>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>12:00 - 12:15<\\/td>\\r\\n\\t<td>\\u041a\\u043e\\u0444\\u0435-\\u0431\\u0440\\u0435\\u0439\\u043a\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>12:15 - 13:00<\\/td>\\r\\n\\t<td>\\u0414\\u0438\\u0441\\u043a\\u0443\\u0441\\u0441\\u0438\\u043e\\u043d\\u043d\\u0430\\u044f \\u0447\\u0430\\u0441\\u0442\\u044c:<ul><li><i><strong><em>\\u0427\\u0442\\u043e \\u0442\\u0430\\u043a\\u043e\\u0435\\r\\n\\u0438\\u0434\\u0435\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043f\\u043e\\u0441\\u0442 \\u0441\\u043e \\u0441\\u0442\\u043e\\u0440\\u043e\\u043d\\u044b \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0438\\u0438\\/\\u0437\\u0430\\u043a\\u0430\\u0437\\u0447\\u0438\\u043a\\u0430\\/\\u0430\\u0432\\u0442\\u043e\\u0440\\u0430. <\\/em><\\/strong><\\/i><\\/li><\\/ul><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>13:00 - 14:00<\\/td>\\r\\n\\t<td>\\u041b\\u0435\\u043a\\u0446\\u0438\\u0438:<ul><li><strong><em>\\u0412\\u044b\\u0441\\u0442\\u0443\\u043f\\u043b\\u0435\\u043d\\u0438\\u0435 \\u0442\\u044c\\u044e\\u0442\\u043e\\u0440\\u043e\\u0432 \\u0421\\u0431\\u0435\\u0440\\u0431\\u0430\\u043d\\u043a\\u0430 \\u043f\\u043e \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u0430\\u043c \\u0411\\u0430\\u043d\\u043a\\u0430<\\/em><\\/strong><\\/li><li><strong><em>\\u0412\\u0432\\u043e\\u0434\\u043d\\u044b\\u0435 \\u043f\\u043e \\u0440\\u0430\\u0431\\u043e\\u0442\\u0435 \\u0432 \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430\\u0445: \\u0438\\u043d\\u0441\\u0442\\u0440\\u0443\\u043a\\u0442\\u0430\\u0436, \\u043e\\u0431\\u0441\\u0443\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 \\u0437\\u0430\\u0434\\u0430\\u043d\\u0438\\u0439<\\/em><\\/strong><\\/li><\\/ul><p><o:p><\\/o:p><\\/p>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>14:30 - 15:00<\\/td>\\r\\n\\t<td>\\u041e\\u0431\\u0435\\u0434\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>15:00 - 16:30\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041b\\u0435\\u043a\\u0446\\u0438\\u0438 \\u043f\\u043e \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430\\u043c<br><br><i><strong>\\u0412\\u044b\\u0441\\u0442\\u0443\\u043f\\u043b\\u0435\\u043d\\u0438\\u0435\\r\\n\\u043f\\u0440\\u0438\\u0433\\u043b\\u0430\\u0448\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0441\\u043f\\u0438\\u043a\\u0435\\u0440\\u043e\\u0432:<ul><li><i><strong>Instagram blogging - \\u043f\\u0440\\u044f\\u043c\\u044b\\u0435 \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0438 \\u0438 \\u0438\\u043c\\u0438\\u0434\\u0436\\u0435\\u0432\\u044b\\u0435 \\u0438\\u0441\\u0442\\u043e\\u0440\\u0438\\u0438<\\/strong><\\/i><\\/li><li><i><strong><i><strong>YouTube vlogging<\\/strong><\\/i><\\/strong><\\/i><\\/li><li><i><strong><i><strong>\\u041c\\u0443\\u043b\\u044c\\u0442\\u0438\\u043c\\u0435\\u0434\\u0438\\u0439\\u043d\\u044b\\u0435 \\u043b\\u043e\\u043d\\u0433\\u0440\\u0438\\u0434\\u044b: \\u043e\\u0442 \\u0434\\u043b\\u0438\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0441\\u0442\\u044b\\u043d\\u0435\\u0439 \\u0438 \\u0444\\u043e\\u0442\\u043e\\u0440\\u0435\\u043f\\u043e\\u0440\\u0442\\u0430\\u0436\\u0435\\u0439 \\u0434\\u043e \\u043f\\u043e\\u043b\\u043d\\u043e\\u0446\\u0435\\u043d\\u043d\\u043e\\u0433\\u043e \\u043d\\u0430\\u0442\\u0438\\u0432\\u043d\\u043e\\u0433\\u043e \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u0430<\\/strong><\\/i><\\/strong><\\/i><\\/li><li><i><strong><i><strong> Facebook\\/vkontakte: \\u0442\\u0440\\u0435\\u043d\\u0434\\u0441\\u0435\\u0442\\u0442\\u0435\\u0440\\u044b \\u0438 \\u043b\\u0438\\u0434\\u0435\\u0440\\u044b \\u043c\\u043d\\u0435\\u043d\\u0438\\u0439 - \\u043f\\u043e\\u0437\\u0438\\u0446\\u0438\\u043e\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u0440\\u0438\\u0432\\u043b\\u0435\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0432\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u044f<\\/strong><\\/i><\\/strong><\\/i><\\/li><\\/ul><\\/strong><\\/i><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>16:30 - 17:00<\\/td>\\r\\n\\t<td>\\u041a\\u043e\\u0444\\u0435-\\u0431\\u0440\\u0435\\u0439\\u043a\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>17:00 - 18:00<\\/td>\\r\\n\\t<td>\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430 \\u0432 \\u0433\\u0440\\u0443\\u043f\\u043f\\u0430\\u0445 \\u043f\\u043e \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430\\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>19:00  <\\/td><td>\\u0423\\u0436\\u0438\\u043d \\u0432 \\u0440\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d\\u0435 \\u00ab\\u0412\\u0435\\u0441\\u043d\\u0430\\u00bb\\r\\n\\t<\\/td>\\r\\n<\\/tr><tr>\\r\\n\\t<td>19:00 - 23:00<\\/td><td>\\u041a\\u043e\\u043c\\u0430\\u043d\\u0434\\u043e\\u043e\\u0431\\u0440\\u0430\\u0437\\u0443\\u044e\\u0449\\u0435\\u0435 \\u043c\\u0435\\u0440\\u043e\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435 (\\u0440\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d \\u00ab\\u0412\\u0435\\u0441\\u043d\\u0430\\u00bb)<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\"}',	1,	1),
(3,	1,	'Образовательный день | Гранд Отель',	'{\"date\":\"24.03.2017\",\"desc\":\"<table>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<td>08:00 - 08:15\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0417\\u0430\\u0440\\u044f\\u0434\\u043a\\u0430\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>08:15 - 10:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0417\\u0430\\u0432\\u0442\\u0440\\u0430\\u043a\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>10:00 - 11:30\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430 \\u0432 \\u0433\\u0440\\u0443\\u043f\\u043f\\u0430\\u0445 \\u043f\\u043e \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430\\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>11:30 - 12:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041a\\u043e\\u0444\\u0435-\\u0431\\u0440\\u0435\\u0439\\u043a\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>12:00 - 13:30\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430 \\u0432 \\u0433\\u0440\\u0443\\u043f\\u043f\\u0430\\u0445 \\u043f\\u043e \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430\\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>13:30 - 14:30\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041e\\u0431\\u0435\\u0434\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>14:30 - 16:30\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430 \\u0432 \\u0433\\u0440\\u0443\\u043f\\u043f\\u0430\\u0445 \\u043f\\u043e \\u043f\\u043e\\u0442\\u043e\\u043a\\u0430\\u043c. \\u0421\\u0434\\u0430\\u0447\\u0430 \\u0440\\u0430\\u0431\\u043e\\u0442\\r\\n\\t<\\/td>\\r\\n<\\/tr><tr>\\r\\n\\t<td>14:30 - 18:00<i><\\/i><\\/td>\\r\\n\\t<td>\\u042d\\u043a\\u0441\\u043a\\u0443\\u0440\\u0441\\u0438\\u044f \\r\\n\\u043d\\u0430 \\u043f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435 \\u00ab\\u041e\\u0431\\u044a\\u0435\\u0434\\u0438\\u043d\\u0435\\u043d\\u043d\\u0430\\u044f \\u0412\\u043e\\u0434\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f\\u00bb \\u0434\\u043b\\u044f \\u0443\\u0447\\u0430\\u0441\\u0442\\u043d\\u0438\\u043a\\u043e\\u0432,\\r\\n\\u0437\\u0430\\u043a\\u043e\\u043d\\u0447\\u0438\\u0432\\u0448\\u0438\\u0445 \\u0441\\u0432\\u043e\\u0438 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b.<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>16:30 - 17:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041a\\u043e\\u0444\\u0435-\\u0431\\u0440\\u0435\\u0439\\u043a\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>17:00 - 18:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041e\\u0442\\u0431\\u043e\\u0440 \\u043b\\u0443\\u0447\\u0448\\u0438\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>19:00 - 19:30\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0422\\u0440\\u0430\\u043d\\u0441\\u0444\\u0435\\u0440\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>19:30 - 23:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0413\\u0430\\u043b\\u0430-\\u0432\\u0435\\u0447\\u0435\\u0440: \\u043f\\u0440\\u0435\\u0437\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u0444\\u0438\\u043d\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0442, \\u0443\\u0436\\u0438\\u043d, \\u0441\\u0442\\u0435\\u043d\\u0434\\u0430\\u043f-\\u0448\\u043e\\u0443 (\\u0425\\u0435\\u0442\\u0430\\u0433, \\u0434\\u0443\\u044d\\u0442 \\u00ab\\u041f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u043e\\u0440 \\u0438 \\u0411\\u043b\\u0438\\u0437\\u00bb)\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\"}',	1,	1),
(4,	1,	'Тимбилдинг',	'{\"date\":\"25.03.2017\",\"desc\":\"<table>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<td>08:00 - 10:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u0417\\u0430\\u0432\\u0442\\u0440\\u0430\\u043a\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>10:00 - 14:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041e\\u0431\\u0440\\u0430\\u0442\\u043d\\u0430\\u044f \\u0441\\u0432\\u044f\\u0437\\u044c, \\u0434\\u0435\\u043b\\u0438\\u043c\\u0441\\u044f \\u0432\\u043f\\u0435\\u0447\\u0430\\u0442\\u043b\\u0435\\u043d\\u0438\\u044f\\u043c\\u0438\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>11:00 - 16:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041a\\u043e\\u043c\\u0430\\u043d\\u0434\\u043e\\u043e\\u0431\\u0440\\u0430\\u0437\\u0443\\u044e\\u0449\\u0435\\u0435 \\u043c\\u0435\\u0440\\u043e\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>13:30 - 14:30\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041e\\u0431\\u0435\\u0434\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>16:00\\r\\n\\t<\\/td>\\r\\n\\t<td>\\u041e\\u0422\\u042a\\u0415\\u0417\\u0414\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\"}',	1,	1);

DROP TABLE IF EXISTS `easyii_faq`;
CREATE TABLE `easyii_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_feedback`;
CREATE TABLE `easyii_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer_subject` varchar(128) DEFAULT NULL,
  `answer_text` text,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_feedback` (`id`, `name`, `email`, `phone`, `title`, `text`, `answer_subject`, `answer_text`, `time`, `ip`, `status`) VALUES
(13,	'Евсюков Сергей',	'oaklandp3@gmail.com',	'+79182723507',	'',	'Мультимедийные лонгриды',	'Ответ на ваше обращение',	'Здравствуйте, Сергей.\r\n\r\nВы успешно зарегистрированы на секцию «Мультимедийные лонгриды»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489665564,	'188.162.167.110',	2),
(14,	'Савичева Александра',	'Asavicheva@gmail.com',	'+79094318990',	'',	'Instagram blogging',	'Ответ на ваше обращение',	'Здравствуйте, Александра\r\n\r\nВы успешно зарегистрированы на секцию «Instagram blogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор Sbercamp.com\r\n',	1489665584,	'217.118.81.229',	2),
(19,	'Эдильсултанов Ризван Ширваниевич',	'Rizvan.82@mail.ru',	'89288924122',	'',	'Instagram blogging',	'Ответ на ваше обращение',	'Здравствуйте, Ризван.\r\n\r\nВы успешно зарегистрированы на секцию «Instagram blogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489666274,	'95.153.132.103',	2),
(20,	'Алимирзаев Муслим',	'msiti210875@gmail.com',	'+79288119495',	'',	'Facebook/vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Муслим.\r\n\r\nВы успешно зарегистрированы на секцию «Facebook/vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489666400,	'37.77.107.195',	2),
(15,	'Веретельник Наталья',	'natalia.veretelnik@gmail.com',	'89094075330',	'',	'YouTube vlogging',	'Ответ на ваше обращение',	'Здравствуйте, Наталья.\r\n\r\nВы успешно зарегистрированы на секцию «YouTube vlogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489665803,	'188.162.166.51',	2),
(16,	'Мартьянова Кристина',	'Redmilk11@gmail.com',	'89996364395',	'',	'Instagram blogging',	'Ответ на ваше обращение',	'Здравствуйте, Кристина.\r\n\r\nВы успешно зарегистрированы на секцию «Instagram blogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489665853,	'37.147.90.47',	2),
(17,	'Каск Елена',	'fluger_po_vetru@mail.ru',	'+7(918)478-17-56',	'',	'Instagram blogging',	'Ответ на ваше обращение',	'Здравствуйте, Елена.\r\n\r\nВы успешно зарегистрированы на секцию «Instagram blogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489666062,	'46.29.12.207',	2),
(18,	'Калибатов Алим',	'tino232@list.ru',	'89280841311',	'',	'Мультимедийные лонгриды',	'Ответ на ваше обращение',	'Здравствуйте, Алим.\r\n\r\nВы успешно зарегистрированы на секцию «Мультимедийные лонгриды»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489666113,	'188.170.196.107',	2),
(21,	'Кочкарова Оксана',	'Okochkarova@mail.ru',	'89054204490',	'',	'Instagram blogging',	'Ответ на ваше обращение',	'Здравствуйте, Оксана.\r\n\r\nВы успешно зарегистрированы на секцию «Instagram blogging\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489666756,	'83.239.118.201',	2),
(22,	'Никитинский Василий',	'fotoblog@list.ru',	'+79209445131',	'',	'Facebook/vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Василий.\r\n\r\nВы успешно зарегистрированы на секцию «Facebook/vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489666958,	'109.229.247.190',	2),
(23,	'Суворов Денис',	'day7726@gmail.com',	'+79283044094',	'',	'YouTube vlogging',	'Ответ на ваше обращение',	'Здравствуйте, Денис.\r\n\r\nВы успешно зарегистрированы на секцию «YouTube vlogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489667129,	'78.106.120.23',	2),
(24,	'Парий Дима',	'pariy@bk.ru',	'89094445000',	'',	'Instagram blogging',	'Ответ на ваше обращение',	'Здравствуйте, Дмитрий.\r\n\r\nВы успешно зарегистрированы на секцию «Instagram blogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489668304,	'95.30.44.68',	2),
(25,	'Заурбеков Ибрагим',	'ibrazaur@yahoo.com',	'89287863333',	'',	'Instagram blogging',	'Ответ на ваше обращение',	'Здравствуйте, Ибрагим.\r\n\r\nВы успешно зарегистрированы на секцию «Instagram blogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489669349,	'95.153.131.7',	2),
(26,	'Стоянов Эрдни',	'logopro.elista@gmail.com',	'+79613979770',	'',	'Facebook/vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Эрдни.\r\n\r\nВы успешно зарегистрированы на секцию «Facebook/vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489671240,	'188.162.50.90',	2),
(27,	'Осканов Яков',	'oskanov@inbox.ru',	'928 902 62 48',	'',	'Facebook/vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Яков.\r\n\r\nВы успешно зарегистрированы на секцию «Facebook/vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489671465,	'188.170.192.74',	2),
(28,	'Соломатин Алексей',	'snesh@mail.ru',	'+79058767474',	'',	'YouTube vlogging',	'Ответ на ваше обращение',	'Здравствуйте, Алексей.\r\n\r\nВы успешно зарегистрированы на секцию «YouTube vlogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489672196,	'31.173.100.195',	2),
(29,	'Тверитнев Анатолий',	'Arc-Slogger@yandex.ru',	'+79209838850',	'',	'Мультимедийные лонгриды',	'Ответ на ваше обращение',	'Здравствуйте, Анатолий.\r\n\r\nВы успешно зарегистрированы на секцию «Мультимедийные лонгриды»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489672200,	'83.149.45.32',	2),
(30,	'Ольга Гнатко',	'gnatkoo@yandex.ru',	'89159957595',	'',	'Facebook/vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Ольга .\r\n\r\nВы успешно зарегистрированы на секцию «Facebook/vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489675734,	'95.108.225.220',	2),
(31,	'Шамиль Амиров',	'amishami.post@gmail.com',	'89883092091',	'',	'YouTube vlogging',	'Ответ на ваше обращение',	'Здравствуйте, Шамиль.\r\n\r\nВы успешно зарегистрированы на секцию «YouTube vlogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489678041,	'194.84.182.10',	2),
(32,	'Долженков Андрей',	'dolgak74@gmail.com',	'8980-242-42-48',	'',	'Facebook/vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Андрей.\r\n\r\nВы успешно зарегистрированы на секцию «Facebook/vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489686599,	'217.25.227.200',	2),
(33,	'Михаил Малышев',	'Mimal.studio@gmail.com',	'89185550613',	'',	'YouTube vlogging',	'Ответ на ваше обращение',	'Здравствуйте, Михаил.\r\n\r\nВы успешно зарегистрированы на секцию «YouTube vlogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489695725,	'46.147.214.13',	2),
(34,	'Мамаева Аня',	'Mamaeva_anna@mail.ru',	'89642299666',	'',	'YouTube vlogging',	'Ответ на ваше обращение',	'Здравствуйте, Анна.\r\n\r\nВы успешно зарегистрированы на секцию «YouTube vlogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489708529,	'80.83.235.110',	2),
(35,	'Жужуев Амар',	'mirotvorec1@yandex.ru',	'+7 909 499 2233',	'',	'YouTube vlogging',	'Ответ на ваше обращение',	'Здравствуйте, Амар.\r\n\r\nВы успешно зарегистрированы на секцию «YouTube vlogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489734243,	'85.115.248.3',	2),
(36,	'Бекмурзиев Мусса',	'mussabek@list.ru',	'+79626442629',	'',	'Facebook / vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Мусса.\r\n\r\nВы успешно зарегистрированы на секцию «Facebook / vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489735576,	'77.87.102.243',	2),
(37,	'Смотрина Ольга',	'Asksmotrina@mail.ru',	'89222100636',	'',	'Facebook / vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Ольга.\r\n\r\nВы успешно зарегистрированы на секцию «Facebook / vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489760197,	'5.189.9.8',	2),
(38,	'Захаров Илья',	'zaharov@stv24.tv',	'+7-905-414-33-63',	'',	'Facebook / vkontakte: трендсеттеры и лидеры мнений',	'Ответ на ваше обращение',	'Здравствуйте, Илья.\r\n\r\nВы успешно зарегистрированы на секцию «Facebook / vkontakte: трендсеттеры и лидеры мнений»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1489999144,	'91.207.137.134',	2),
(40,	'Самойлик Станислав',	'pierwszy@yandex.ru',	'89185250779',	'',	'Мультимедийные лонгриды',	'Ответ на ваше обращение',	'Здравствуйте, Станислав.\r\n\r\nВы успешно зарегистрированы на секцию «Мультимедийные лонгриды»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1490000642,	'194.84.182.10',	2),
(41,	'Руслан Аларханов',	'alarhanov94@gmail.com',	'89322473722',	'',	'YouTube vlogging',	'Ответ на ваше обращение',	'Здравствуйте, Руслан.\r\n\r\nВы успешно зарегистрированы на секцию «YouTube vlogging»\r\n\r\n\r\nС наилучшими пожеланиями,\r\nадминистратор sbercamp.com\r\n',	1490025963,	'194.84.182.10',	2),
(42,	'Руфова Елена',	'eyrufova@sberbank.ru',	'89081799657',	'',	'Мультимедийные лонгриды',	NULL,	NULL,	1490279556,	'95.153.131.37',	1);

DROP TABLE IF EXISTS `easyii_files`;
CREATE TABLE `easyii_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `file` varchar(255) NOT NULL,
  `size` int(11) DEFAULT '0',
  `slug` varchar(128) DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_gallery_categories`;
CREATE TABLE `easyii_gallery_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_gallery_categories` (`id`, `title`, `description`, `image_file`, `slug`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(1,	'Галерея',	'',	NULL,	'gallery',	1,	1,	2,	0,	1,	1);

DROP TABLE IF EXISTS `easyii_guestbook`;
CREATE TABLE `easyii_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer` text,
  `email` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_loginform`;
CREATE TABLE `easyii_loginform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `user_agent` varchar(1024) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_loginform` (`id`, `username`, `password`, `ip`, `user_agent`, `time`, `success`) VALUES
(1,	'root',	'******',	'127.0.0.1',	'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',	1489510735,	1),
(2,	'root@site-control.ru',	'XySoIQJo',	'127.0.0.1',	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0',	1489560994,	0),
(3,	'root@site-control.ru',	'XySoIQJo',	'127.0.0.1',	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0',	1489560996,	0),
(4,	'root',	'******',	'127.0.0.1',	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0',	1489561004,	1),
(5,	'root',	'******',	'127.0.0.1',	'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',	1489571936,	1),
(6,	'root',	'******',	'212.192.204.44',	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0',	1489581820,	1),
(7,	'root',	'******',	'212.192.204.45',	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 OPR/43.0.2442.1144',	1489584232,	1),
(8,	'admin@admin.ru ',	'QVEYPpBLN4',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',	1489653179,	0),
(9,	'admin@admin.ru',	'QVEYPpBLN4',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489653281,	0),
(10,	'root',	'******',	'212.192.204.45',	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 OPR/43.0.2442.1144',	1489653446,	1),
(11,	'root',	'XySolQJo',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489653536,	0),
(12,	'root',	'******',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489653556,	1),
(13,	'root',	'XySoLQJo',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',	1489662665,	0),
(14,	'root',	'XySolQJo',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',	1489662689,	0),
(15,	'root',	'XySolQJo',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',	1489662709,	0),
(16,	'root ',	'XySolQJo',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',	1489662745,	0),
(17,	'root ',	'XySoIQJo',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',	1489662775,	0),
(18,	'root ',	'XySoIJo',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',	1489662795,	0),
(19,	'root',	'XySoIJo',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',	1489663705,	0),
(20,	'root',	'******',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',	1489663737,	1),
(21,	'root',	'******',	'46.147.120.150',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489695666,	1),
(22,	'root',	'******',	'188.168.215.29',	'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0',	1489700813,	1),
(23,	'root',	'XySoLQJo',	'2.93.157.97',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489773630,	0),
(24,	'root',	'******',	'2.93.157.97',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489773650,	1),
(25,	'admin',	'admin',	'212.90.61.85',	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489782051,	0),
(26,	'admin',	'root/XySoIQJo',	'212.90.61.85',	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489782187,	0),
(27,	'admin',	'XySoIQJo',	'212.90.61.85',	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489782195,	0),
(28,	'root',	'******',	'212.90.61.85',	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489782198,	1),
(29,	'root',	'******',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1489989868,	1),
(30,	'sbercamp_admin',	'9kw3dMEM',	'188.168.215.29',	'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0',	1490036387,	0),
(31,	'root',	'9kw3dMEM',	'188.168.215.29',	'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0',	1490036394,	0),
(32,	'root',	'******',	'188.168.215.29',	'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0',	1490036428,	1),
(33,	'root',	'******',	'212.192.204.44',	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0',	1490093290,	1),
(34,	'root',	'******',	'212.192.204.44',	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0',	1490271188,	1),
(35,	'root',	'******',	'194.84.182.10',	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',	1490280091,	1);

DROP TABLE IF EXISTS `easyii_menu`;
CREATE TABLE `easyii_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(128) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `items` text,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_menu` (`menu_id`, `slug`, `title`, `items`, `status`) VALUES
(1,	'main',	'Главное меню',	'[\n    {\n        \"label\": \"О проекте\",\n        \"url\": \"/#o-proekte\"\n    },\n    {\n        \"label\": \"Спикеры\",\n        \"url\": \"/#spikery\"\n    },\n    {\n        \"label\": \"Расписание\",\n        \"url\": \"/#raspisanie\"\n    },\n    {\n        \"label\": \"Регистрация\",\n        \"url\": \"/#registracia\"\n    },\n    {\n        \"label\": \"Участники\",\n        \"url\": \"/uchastniki\"\n    },\n    {\n        \"label\": \"Контакты\",\n        \"url\": \"/#kontakty\"\n    }\n]',	1);

DROP TABLE IF EXISTS `easyii_migration`;
CREATE TABLE `easyii_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `easyii_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1489510732),
('m000000_000000_install',	1489510733),
('m000009_100000_update',	1489510734),
('m000009_200000_update',	1489510734),
('m000009_200003_module_menu',	1489510734),
('m000009_200004_update',	1489510734);

DROP TABLE IF EXISTS `easyii_modules`;
CREATE TABLE `easyii_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `class` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `settings` text,
  `notice` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_modules` (`id`, `name`, `class`, `title`, `icon`, `settings`, `notice`, `order_num`, `status`) VALUES
(1,	'entity',	'yii\\easyii\\modules\\entity\\EntityModule',	'Объекты',	'asterisk',	'{\"categoryThumb\":true,\"categorySlugImmutable\":false,\"categoryDescription\":true,\"itemsInFolder\":false}',	0,	90,	1),
(2,	'article',	'yii\\easyii\\modules\\article\\ArticleModule',	'Статьи',	'pencil',	'{\"categoryThumb\":true,\"categorySlugImmutable\":false,\"categoryDescription\":true,\"articleThumb\":true,\"enablePhotos\":true,\"enableTags\":true,\"enableShort\":true,\"shortMaxLength\":255,\"itemsInFolder\":false,\"itemSlugImmutable\":false}',	0,	60,	0),
(3,	'carousel',	'yii\\easyii\\modules\\carousel\\CarouselModule',	'Видео со Sbercamp',	'picture',	'{\"enableTitle\":true,\"enableText\":true}',	0,	40,	1),
(4,	'catalog',	'yii\\easyii\\modules\\catalog\\CatalogModule',	'Секции',	'list-alt',	'{\"categoryThumb\":false,\"categorySlugImmutable\":false,\"categoryDescription\":false,\"itemsInFolder\":false,\"itemThumb\":true,\"itemPhotos\":true,\"itemDescription\":true,\"itemSlugImmutable\":false}',	0,	95,	1),
(5,	'faq',	'yii\\easyii\\modules\\faq\\FaqModule',	'Вопросы и ответы',	'question-sign',	'{\"questionHtmlEditor\":true,\"answerHtmlEditor\":true,\"enableTags\":true}',	0,	45,	0),
(6,	'feedback',	'yii\\easyii\\modules\\feedback\\FeedbackModule',	'Заявки на участие',	'earphone',	'{\"mailAdminOnNewFeedback\":false,\"subjectOnNewFeedback\":\"New feedback\",\"templateOnNewFeedback\":\"@easyii\\/modules\\/feedback\\/mail\\/en\\/new_feedback\",\"answerTemplate\":\"@easyii\\/modules\\/feedback\\/mail\\/en\\/answer\",\"answerSubject\":\"Answer on your feedback message\",\"answerHeader\":\"Hello,\",\"answerFooter\":\"Best regards.\",\"telegramAdminOnNewFeedback\":false,\"telegramTemplateOnNewFeedback\":\"@easyii\\/modules\\/feedback\\/telegram\\/en\\/new_feedback\",\"enableTitle\":false,\"enableEmail\":true,\"enablePhone\":true,\"enableText\":true,\"enableCaptcha\":false}',	0,	100,	1),
(7,	'file',	'yii\\easyii\\modules\\file\\FileModule',	'Файлы',	'floppy-disk',	'{\"slugImmutable\":false}',	0,	30,	0),
(8,	'gallery',	'yii\\easyii\\modules\\gallery\\GalleryModule',	'Фотогалерея',	'camera',	'{\"categoryThumb\":true,\"itemsInFolder\":false,\"categoryTags\":true,\"categorySlugImmutable\":false,\"categoryDescription\":true}',	0,	80,	1),
(9,	'guestbook',	'yii\\easyii\\modules\\guestbook\\GuestbookModule',	'Гостевая книга',	'book',	'{\"enableTitle\":false,\"enableEmail\":true,\"preModerate\":false,\"enableCaptcha\":false,\"mailAdminOnNewPost\":true,\"subjectOnNewPost\":\"New message in the guestbook.\",\"templateOnNewPost\":\"@easyii\\/modules\\/guestbook\\/mail\\/en\\/new_post\",\"frontendGuestbookRoute\":\"\\/guestbook\",\"subjectNotifyUser\":\"Your post in the guestbook answered\",\"templateNotifyUser\":\"@easyii\\/modules\\/guestbook\\/mail\\/en\\/notify_user\"}',	0,	70,	0),
(10,	'menu',	'yii\\easyii\\modules\\menu\\MenuModule',	'Меню',	'menu-hamburger',	'{\"slugImmutable\":false}',	0,	51,	1),
(11,	'news',	'yii\\easyii\\modules\\news\\NewsModule',	'Новости',	'bullhorn',	'{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true,\"slugImmutable\":false}',	0,	65,	0),
(12,	'page',	'yii\\easyii\\modules\\page\\PageModule',	'Страницы',	'file',	'{\"slugImmutable\":true,\"defaultFields\":\"[]\"}',	0,	50,	1),
(13,	'shopcart',	'yii\\easyii\\modules\\shopcart\\ShopcartModule',	'Заказы',	'shopping-cart',	'{\"mailAdminOnNewOrder\":true,\"subjectOnNewOrder\":\"New order\",\"templateOnNewOrder\":\"@easyii\\/modules\\/shopcart\\/mail\\/en\\/new_order\",\"subjectNotifyUser\":\"Your order status changed\",\"templateNotifyUser\":\"@easyii\\/modules\\/shopcart\\/mail\\/en\\/notify_user\",\"frontendShopcartRoute\":\"\\/shopcart\\/order\",\"enablePhone\":true,\"enableEmail\":true}',	0,	120,	0),
(14,	'subscribe',	'yii\\easyii\\modules\\subscribe\\SubscribeModule',	'E-mail рассылка',	'envelope',	'[]',	0,	10,	0),
(15,	'text',	'yii\\easyii\\modules\\text\\TextModule',	'Текстовые блоки',	'font',	'[]',	0,	20,	0);

DROP TABLE IF EXISTS `easyii_news`;
CREATE TABLE `easyii_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_pages`;
CREATE TABLE `easyii_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT '0',
  `fields` text,
  `data` text,
  `tree` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  `depth` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_pages` (`id`, `title`, `text`, `slug`, `show_in_menu`, `fields`, `data`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(1,	'Главная',	'',	'index',	0,	'[{\"name\":\"address\",\"title\":\"\\u0410\\u0434\\u0440\\u0435\\u0441\",\"type\":\"address\",\"options\":\"\"}]',	'{\"address\":\"\\u041a\\u0443\\u0440\\u043e\\u0440\\u0442\\u043d\\u044b\\u0439 \\u0431-\\u0440, 14, \\u041a\\u0438\\u0441\\u043b\\u043e\\u0432\\u043e\\u0434\\u0441\\u043a, \\u0421\\u0442\\u0430\\u0432\\u0440\\u043e\\u043f\\u043e\\u043b\\u044c\\u0441\\u043a\\u0438\\u0439 \\u043a\\u0440\\u0430\\u0439, \\u0420\\u043e\\u0441\\u0441\\u0438\\u044f, 357700#ChIJJ7MDqXIrWEARrxwU4ofG3R0#43.8994499#42.716331500000024\"}',	1,	1,	2,	0,	1,	1),
(2,	'Участники',	'',	'ucastniki',	0,	'{}',	'{}',	2,	1,	2,	0,	2,	1);

DROP TABLE IF EXISTS `easyii_photos`;
CREATE TABLE `easyii_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `image_file` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `model_item` (`class`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_photos` (`id`, `class`, `item_id`, `image_file`, `description`, `order_num`) VALUES
(17,	'yii\\easyii\\modules\\gallery\\models\\Category',	1,	'gallery/3-c4be80123324fc-d7a018e755.jpg',	'',	8),
(11,	'yii\\easyii\\modules\\gallery\\models\\Category',	1,	'gallery/6-998f227848.jpg',	'',	3),
(12,	'yii\\easyii\\modules\\gallery\\models\\Category',	1,	'gallery/3-c4be8014fc.jpg',	'',	5),
(13,	'yii\\easyii\\modules\\gallery\\models\\Category',	1,	'gallery/4-0c565e2099.jpg',	'',	4),
(14,	'yii\\easyii\\modules\\gallery\\models\\Category',	1,	'gallery/2-81d65bc01d.jpg',	'',	9),
(15,	'yii\\easyii\\modules\\gallery\\models\\Category',	1,	'gallery/5-0e2e7955d6.jpg',	'',	6),
(16,	'yii\\easyii\\modules\\gallery\\models\\Category',	1,	'gallery/unnamed-02a8bedf72.jpg',	'',	7);

DROP TABLE IF EXISTS `easyii_seotext`;
CREATE TABLE `easyii_seotext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_item` (`class`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_seotext` (`id`, `class`, `item_id`, `h1`, `title`, `keywords`, `description`) VALUES
(1,	'yii\\easyii\\modules\\page\\models\\Page',	1,	'SBERCAMP',	'SBERCAMP',	'SBERCAMP',	'SBERCAMP'),
(2,	'yii\\easyii\\modules\\page\\models\\Page',	2,	'Участники SBERCAMP',	'Участники SBERCAMP',	'Участники SBERCAMP',	'Участники SBERCAMP');

DROP TABLE IF EXISTS `easyii_settings`;
CREATE TABLE `easyii_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `title` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `easyii_settings` (`id`, `name`, `title`, `value`, `visibility`) VALUES
(1,	'easyii_version',	'EasyiiCMS version',	'0.91',	0),
(2,	'image_max_width',	'Максимальная ширина загружаемых изображений, которые автоматически не сжимаются',	'1900',	2),
(3,	'redactor_plugins',	'Список плагинов редактора Redactor через запятую',	'imagemanager, filemanager, table, fullscreen',	1),
(4,	'ga_service_email',	'E-mail сервис аккаунта Google Analytics',	'',	1),
(5,	'ga_profile_id',	'Номер профиля Google Analytics',	'',	1),
(6,	'ga_p12_file',	'Путь к файлу ключей p12 сервис аккаунта Google Analytics',	'',	1),
(7,	'gm_api_key',	'Google Maps API ключ',	'AIzaSyD3TNxUzBswDgaODRYXWsHdWgfwe-i3Vm0',	1),
(8,	'recaptcha_key',	'ReCaptcha key',	'',	1),
(9,	'password_salt',	'Password salt',	'K5zlGqNaDOcLVP3b4nCA_aIirAW3H_yz',	0),
(10,	'root_auth_key',	'Root authorization key',	'-BpyRK2uAMmrPFmSHuWx9VrLyCTdhAD5',	0),
(11,	'root_password',	'Пароль разработчика',	'86aadc8e0e9299ed955735efd7e2ba51b94bf88d',	1),
(12,	'auth_time',	'Время авторизации',	'86400',	1),
(13,	'robot_email',	'E-mail рассыльщика',	'sbercamp@yandex.ru',	1),
(14,	'admin_email',	'E-mail администратора',	'vpselivanova@sberbank.ru',	2),
(15,	'telegram_chat_id',	'Telegram chat ID',	'',	2),
(16,	'telegram_bot_token',	'Telegram bot token',	'',	1),
(17,	'recaptcha_secret',	'ReCaptcha secret',	'',	1),
(18,	'toolbar_position',	'Позиция панели на сайте (\"top\" or \"bottom\" or \"hide\")',	'hide',	1),
(19,	'repeat_email',	'Email для дублирования',	'radavtyan@sberbank.ru',	2);

DROP TABLE IF EXISTS `easyii_shopcart_goods`;
CREATE TABLE `easyii_shopcart_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL,
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_shopcart_orders`;
CREATE TABLE `easyii_shopcart_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `address` varchar(1024) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `comment` varchar(1024) DEFAULT NULL,
  `remark` varchar(1024) DEFAULT NULL,
  `access_token` varchar(32) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_subscribe_history`;
CREATE TABLE `easyii_subscribe_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(128) NOT NULL,
  `body` text,
  `sent` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_subscribe_subscribers`;
CREATE TABLE `easyii_subscribe_subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_tags`;
CREATE TABLE `easyii_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `frequency` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_tags_assign`;
CREATE TABLE `easyii_tags_assign` (
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `class` (`class`),
  KEY `item_tag` (`item_id`,`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `easyii_texts`;
CREATE TABLE `easyii_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- 2017-03-23 18:57:19
