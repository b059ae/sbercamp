<?php
namespace app\helpers;

class Html extends \yii\helpers\Html
{
    /**
     * Заменяет http на ссылки
     * @param $str
     * @return mixed
     */
    public static function replaceLink($str){
        $str = str_replace('"', "'", $str);
        return preg_replace('/((www|http:\/\/|https:\/\/)[^ ]+)/si', "<a href='$1' target='_blank'>$1</a>", $str);
    }
}