<?php

namespace app\controllers;

use app\models\FeedbackForm;
use app\models\SignupForm;
use Yii;
use yii\easyii\helpers\Mail;
use yii\easyii\models\Setting;
use yii\easyii\modules\carousel\api\Carousel;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\entity\api\Entity;
use yii\easyii\modules\gallery\api\Gallery;
use yii\easyii\modules\page\api\Page;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        $page = Page::get('index');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        
        return $this->render('index',[
            'page' => $page,
            'gallery' => Gallery::cat('gallery')->getPhotos([
                'pagination'=>[
                    'pageSize'=>1000,
                ],
            ]),
            'spikery' => Catalog::cat('spikery')->getItems(),
            'raspisanie' => Entity::cat('raspisanie')->getItems(),
            'video' => Carousel::items(),
            /*'signupForm' => new SignupForm(),*/
            'feedbackForm' => new FeedbackForm(),
        ]);
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionUchastniki()
    {
        $page = Page::get('ucastniki');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }

        return $this->render('uchastniki',[
            'page' => $page,
            'ucastniki' => Catalog::cat('ucastniki')->getChildren(),
            'gallery'=>[
                'instagram-blogging' => Gallery::cat('instagram-blogging')->getPhotos([
                    'pagination'=>[
                        'pageSize'=>1000,
                    ],
                ]),
                'youtube-vlogging' => Gallery::cat('youtube-vlogging')->getPhotos([
                    'pagination'=>[
                        'pageSize'=>1000,
                    ],
                ]),
                'multimediynie-longridy' => Gallery::cat('multimediynie-longridy')->getPhotos([
                    'pagination'=>[
                        'pageSize'=>1000,
                    ],
                ]),
                'facebookvkontakte' => Gallery::cat('facebookvkontakte')->getPhotos([
                    'pagination'=>[
                        'pageSize'=>1000,
                    ],
                ]),
            ]
        ]);
    }

    /**
     * Регистрация
     * @return string
     */
    public function actionSignup(){
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Ваша заявка принята. Администратор в ближайшее время ее подтвердит.');
                Mail::send(
                    Setting::get('admin_email'),
                    'Регистрация участника SBERCAMP',
                    '@app/mail/signup',
                    [
                        'model' => $model,
                        'link' => Url::to(['/admin/feedback'], true)
                    ],
                    [
                        'cc'=>Setting::get('repeat_email'),
                    ]
                );
            }
        }
        return $this->redirect('/');
    }

    /**
     * Отзыв
     * @return string
     */
    public function actionFeedback(){
        $model = new FeedbackForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', 'Спасибо, Ваш отзыв принят.');
            /*Mail::send(
                Setting::get('admin_email'),
                'Отзыв участника SBERCAMP',
                '@app/mail/feedback',
                [
                    'model' => $model,
                ],
                [
                    'cc'=>Setting::get('repeat_email'),
                ]
            );*/

        }
        return $this->redirect('/');

    }
}